﻿
using Code.Core;
#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

[RequireComponent(typeof(Text))]
public class Localize : MonoBehaviour
{
    [SerializeField] public string key;

    Text textToLocalize;

	// Use this for initialization
	void Start ()
    {
        textToLocalize = GetComponent<Text>();
        LocalizeText();
        Language.OnLanguageChanged += LocalizeText;
    }

    void LocalizeText()
    {
        textToLocalize.SetTextByKey(key);
    }
}

[CustomEditor(typeof(Localize))]
public class LocalizeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Localize myScript = (Localize)target;
        string localizedText = Language.GetLocalizationByKey(myScript.key);
        localizedText = string.IsNullOrEmpty(localizedText) ? "No such loc key" : localizedText;
        GUILayout.Label(localizedText);
    }
}
#endif