﻿using System;
using Code.Core.Managers;
using UnityEngine;

namespace Code.UI {
    public class InputLockController : MonoBehaviour
    {
        [SerializeField] CanvasGroup screenCanvasGroup;

        [Inject] InputManager m_InputManager;
        bool m_IsCurrentlyLocked;

        void Awake()
        {
            this.Inject();
        }

        // Update is called once per frame
        void Update()
        {
            bool localLock = m_InputManager.IsInputLock();
            if (localLock != m_IsCurrentlyLocked)
                if (localLock)
                    Lock();
                else
                    Unlock();
        }

        void Lock()
        {
            m_IsCurrentlyLocked = true;
            screenCanvasGroup.interactable = false;
            screenCanvasGroup.blocksRaycasts = false;
        }

        void Unlock()
        {
            m_IsCurrentlyLocked = false;
            screenCanvasGroup.interactable = true;
            screenCanvasGroup.blocksRaycasts = true;
        }
    }
}
