﻿using System.Collections;
using System.Collections.Generic;
using Code.Core;
using UnityEngine;

public class TestScreen : ScreenBase
{
    private void Start()
    {
        //Invoke("SS", 3);
        //Invoke("SS2", 5);
        //Invoke("SS", 7);
        //Invoke("SS2", 10);
        //Invoke("SS", 12);
    }
    void SS()
    {
        //HideScreen();
        PlayAnimation("TestScreen_Show");
    }
    void SS2()
    {
        //HideScreen();
        PlayAnimation("TestScreen_Hide");
    }
    public void PrintHui()
    {
        Log.Info("printHui");
    }
    public void ChangeLanguageEn()
    {
        Language.ChangeLanguage(Language.Languages.English);
    }
    public void ChangeLanguageRu()
    {
        Language.ChangeLanguage(Language.Languages.Russian);
    }
}
