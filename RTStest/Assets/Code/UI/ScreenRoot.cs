﻿using Code.Core.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Code.UI {
    public class ScreenRoot : MonoBehaviour
    {
        [Inject] ScreenManager m_ScreenManager;
        
        [SerializeField] public RectTransform screenTransform;

        void Awake()
        {
            this.Inject();
            m_ScreenManager.SetScreenRoot(this);
        }

        public T SpawnScreen<T>(RectTransform screenPrefab) where T : ScreenBase
        {
            return Instantiate(screenPrefab, screenTransform).GetComponent<T>();
        }
    }
}
