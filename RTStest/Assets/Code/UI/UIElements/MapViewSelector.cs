﻿using UnityEngine;

namespace Code.UI.UIElements {
    public class MapViewSelector : MonoBehaviour
    {
        [SerializeField] GameObject smallMap;
        [SerializeField] GameObject bigMap;

        public void OnExpandClick()
        {
            smallMap.gameObject.SetActive(false);
            bigMap.gameObject.SetActive(true);
        }

        public void OnMinimizeClick()
        {
            smallMap.gameObject.SetActive(true);
            bigMap.gameObject.SetActive(false);
        }
    }
}
