﻿using System;
using Code.Core.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Code.UI.UIElements {
    public class MapView : MonoBehaviour
    {
        [Inject] MapManager m_MapManager;
        [Inject] ScreenManager m_ScreenManager;
        [Inject] InputManager m_InputManager;
        [Inject] PlayerManager m_PlayerManager;
        
        [SerializeField] RectTransform uiMapTransform;
        [SerializeField] RectTransform currentPos;
        [SerializeField] float screenSizePercentage = 0.33f;
        
        const float REFERENCE_CAMERA_POS_VIEW_SIZE = 40f;
        const float MIN_CAMERA_POS_VIEW_PERCENT = 0.05f;

        float m_AdjustedMapPosSize;
        Vector2 m_CurrentMapPos;
        bool m_Initialized;

        void Start()
        {
            Init();
        }

        void Init()
        {
            this.Inject();
            Vector2 referenceResolution = new Vector2(m_ScreenManager.ScreenRoot.screenTransform.rect.width, m_ScreenManager.ScreenRoot.screenTransform.rect.height);
            float xyRatio = m_MapManager.MapBound.Bounds.x / m_MapManager.MapBound.Bounds.z;
            float mapHeight = Mathf.Min(referenceResolution.y * screenSizePercentage, m_ScreenManager.ScreenRoot.screenTransform.rect.height - 2 * uiMapTransform.anchoredPosition.y);
            float mapWidth =  Mathf.Min(mapHeight * xyRatio, m_ScreenManager.ScreenRoot.screenTransform.rect.width - 2 * uiMapTransform.anchoredPosition.x);
            uiMapTransform.sizeDelta = new Vector2(mapWidth, mapHeight);
            m_AdjustedMapPosSize = Mathf.Max(REFERENCE_CAMERA_POS_VIEW_SIZE / m_MapManager.MapBound.Bounds.x * mapWidth, MIN_CAMERA_POS_VIEW_PERCENT * mapWidth);
            float size = m_AdjustedMapPosSize * m_PlayerManager.CameraController.RelativeHeight;
            m_CurrentMapPos = new Vector2(size, size);
            m_Initialized = true;
        }
        
        public void OnMapClicked()
        {
            if (!Input.GetButton("Fire1"))
                return;
            Vector2 clickPosition = m_InputManager.GetScaledMousePosition() - uiMapTransform.anchoredPosition;
            m_MapManager.OnUIMapClicked(clickPosition, uiMapTransform);
            currentPos.anchoredPosition = clickPosition;
        }

        void FixedUpdate()
        {
            if (m_Initialized)
                UpdateView();
        }

        void OnEnable()
        {
            if (m_Initialized)
                UpdateView();
        }

        void UpdateView()
        {
            SetCurrentPosition();
            SetMapPosSize();
            SetMapPosRotation();
        }

        void SetCurrentPosition()
        {
            Vector2 relativeMapPosition = m_MapManager.GetCurrentRelativePositionOnMap();
            currentPos.anchoredPosition = new Vector2(relativeMapPosition.x * uiMapTransform.sizeDelta.x, relativeMapPosition.y * uiMapTransform.sizeDelta.y);
        }

        void SetMapPosSize()
        {
            float size = m_AdjustedMapPosSize * m_PlayerManager.CameraController.RelativeHeight;
            m_CurrentMapPos.Set(size, size);
            currentPos.sizeDelta = m_CurrentMapPos;
        }
        
        void SetMapPosRotation()
        {
            currentPos.rotation =  Quaternion.Euler(0, 0, -m_PlayerManager.CameraController.CameraRotation);
        }
    }
}
