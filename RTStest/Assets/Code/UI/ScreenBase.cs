﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScreenBase : MonoBehaviour
{
    [SerializeField] AnimatorController animatorController;

    public Action OnShown;
    public Action OnHidden;

    Action OnShownInternal;
    Action OnHiddenInternal;

    public void Init()
    {
        if (animatorController == null)
            animatorController = GetComponent<AnimatorController>();
        animatorController.Init();
        InternalInit();
    }

    protected virtual void InternalInit()
    {

    }

    public void ShowScreen(Action onShowed = null)
    {
        OnShownInternal = onShowed;
        PlayShowAnim();
    }

    public void HideScreen(Action onHidden = null)
    {
        OnHiddenInternal = onHidden;
        PlayHideAnim();
    }

    public void PlayAnimation(string animName, Action OnAnimated = null)
    {
        animatorController.Play(animName, () => OnAnimated.Fire());
    }

    void PlayShowAnim()
    {
        string showAnimName = GetType().ToString() + "_Show";
        if (animatorController != null && animatorController.HasAnimation(showAnimName))
            PlayAnimation(showAnimName, Shown);
        else
            ShowFade();
    }

    void PlayHideAnim()
    {
        string hideAnimName = GetType().ToString() + "_Hide";
        if (animatorController != null && animatorController.HasAnimation(hideAnimName))
            PlayAnimation(hideAnimName, Hidden);
        else
            HideFade();
    }

    void ShowFade()
    {
        Shown();
    }

    void HideFade()
    {
        Hidden();
    }

    void Shown()
    {
        OnShown.Fire();
        OnShownInternal.Fire();
        OnShownInternal = null;
    }

    void Hidden()
    {
        OnHidden.Fire();
        OnHiddenInternal.Fire();
        OnHiddenInternal = null;
    }
}
