﻿#if UNITY_EDITOR
using Code.Core.Tools;
using UnityEngine;
using UnityEditor;

namespace Code.Editor.Tools {
    [CustomEditor(typeof(VisibilityGridGenerator))]
    public class VisibilityGridGeneratorEditor : UnityEditor.Editor
    {
        VisibilityGridGenerator visibilityGridGenerator 
        {
            get { return (VisibilityGridGenerator) serializedObject.targetObject; }
        }
    
        SerializedProperty visibilitySquareSize;
        SerializedProperty maximumVisibility;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();

            if (GUILayout.Button("Generate"))
                visibilityGridGenerator.Generate();
        }
    }
}
#endif
