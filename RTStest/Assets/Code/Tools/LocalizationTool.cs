﻿
using Code.Core;
#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System.Text;

public class LocalizationTool : MonoBehaviour
{
    const string LOCALIZATION_PATH = "Assets/Resources/UI/Localization/Localization.txt"; 

    [MenuItem("Tools/Localization/Generate Localization Files")]
    public static void GenerateLocalizationFiles()
    {
        Encoding enc = Encoding.GetEncoding(1251);
        string[] lines = File.ReadAllLines(LOCALIZATION_PATH, enc);

        List<string> enLoc = new List<string>();
        List<string> eeLoc = new List<string>();
        List<string> ruLoc = new List<string>();

        for (int i = 1; i < lines.Length; i++)
        {
            string[] locLine = CutLocalizationLine(lines[i]);
            enLoc.Add(locLine[0]);
            ruLoc.Add(locLine[0]);
            eeLoc.Add(locLine[0]);

            enLoc.Add(locLine[1]);
            ruLoc.Add(locLine[2]);
            eeLoc.Add(locLine[3]);
        }
        File.WriteAllLines(Language.GetLanguageFilePath(Language.Languages.English), enLoc.ToArray());
        File.WriteAllLines(Language.GetLanguageFilePath(Language.Languages.Russian), ruLoc.ToArray());
        File.WriteAllLines(Language.GetLanguageFilePath(Language.Languages.Estonian), eeLoc.ToArray());
    }

    static string[] CutLocalizationLine(string line)
    {
        string[] lines = line.Split('"');
        List<string> result = new List<string>();
        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i] != " " && !string.IsNullOrEmpty(lines[i]))
                result.Add(lines[i]);
        }
        return result.ToArray();
    }
}
#endif
