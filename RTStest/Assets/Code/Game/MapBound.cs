﻿using System;
using Code.Core.Managers;
using UnityEngine;

namespace Code.Game {
    [ExecuteInEditMode]
    public class MapBound : MonoBehaviour
    {
        [Inject] MapManager m_MapManager;
        
//        [SerializeField] float extendX;
//        [SerializeField] float extendY;
//        [SerializeField] float extendZ;

        const float BOUND_HEIGHT = 20f;

        [SerializeField] Vector3 m_Bounds;
        public Vector3 Bounds { get { return m_Bounds; } }

        void Awake()
        {
            this.Inject();
            //m_Bounds = new Vector3(extendX, BOUND_HEIGHT, extendZ);
            m_MapManager.SetMapBound(this);
        }

        void Update()
        {
//            if (!Debug.isDebugBuild)
//                return;
//            if (Math.Abs(m_Bounds.x - extendX) > 1f || Math.Abs(m_Bounds.z - extendZ) > 1f)
//                m_Bounds.Set(extendX, BOUND_HEIGHT, extendZ);
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(transform.position, m_Bounds);
        }
    }
}
