﻿namespace Code.Game.Model.Units {
    public enum DamageSeverity
    {
        NoDamage = 0,
        Small = 1,
        Medium = 2,
        Critical = 3,
        Destroyed = 4
    }
}
