﻿using Code.Core;
using Code.Game.Model.Units.Tasks;
using UnityEngine;

namespace Code.Game.Model.Units {
    public abstract class UnitAI : MonoBehaviour
    {
        public bool Active { get; private set; }
        protected bool Inited { get; private set; }
        
        protected UnitBody Body;
        protected UnitDetailsOld DetailsOld;
        protected TaskBase MasterTask;
        protected TaskBase CurrentTask;

        public bool IsExecutingTask
        {
            get { return (CurrentTask != null) && CurrentTask.InProgress; }
        }
        
        protected virtual void ExecuteCurrentTask() { }
        protected virtual void CheckEnvironment() { }
        protected virtual void InitInternal() { }
        
        public void Init(UnitBody controlledBody, UnitDetailsOld unitDetailOld)
        {
            Body = controlledBody;
            DetailsOld = unitDetailOld;
            InitInternal();
            Inited = true;
            Activate();
        }

        public void Activate()
        {
            Active = true;
        }

        public void ExecuteTask(TaskBase task)
        {
            if (!CanCompleteTask(task))
            {
                Log.Warning("Unit can't complete task");
                task.AbortTask();
                return;
            }
            if (CurrentTask != null && CurrentTask.InProgress)
            {
                Log.Warning("Previous task not completed!");
                CurrentTask?.AbortTask();
            }

            MasterTask = task;
            CurrentTask = MasterTask.GetActualTask();
            if (task is ComplexTaskBase complexTask)
            {
                complexTask.SubtaskCompleted += () => CurrentTask = complexTask.GetActualTask();
                complexTask.Start();
            }
        }
        
        void FixedUpdate()
        {
            if (!Active)
                return;
            CheckEnvironment();
            if (Body == null || !Body.Inited)
                return;
            if (CurrentTask != null && CanProceedTask())
                ExecuteCurrentTask();
            if (MasterTask != null)
                CheckMasterTask();
        }

        protected bool CanProceedTask()
        {
            if (!CurrentTask.TaskCanBeDone(DetailsOld))
            {
                Log.Warning($"Unity {DetailsOld.UnitName} Task cannot be executed {CurrentTask.Description}");
                CurrentTask.AbortTask();
                return false;
            }
            if (CurrentTask.InProgress && CurrentTask.CompleteConditionCheck())
            {
                Log.Info($"Unity {DetailsOld.UnitName} Task completed {CurrentTask.Description}");
                CurrentTask.Complete();
                return false;
            }
            return true;
        }

        public void StopTask()
        {
            CurrentTask?.StopTask();
        }

        public virtual bool CanCompleteTask(TaskBase task)
        {
            task.TaskCanBeDone(DetailsOld);
            return true;
        } 

        protected void CheckMasterTask()
        {
            if (MasterTask.InProgress && MasterTask.CompleteConditionCheck())
            {
                Log.Info($"Master task completed {DetailsOld.UnitName} Task completed {MasterTask.Description}");
                MasterTask.Complete();
            }
        }
        
        public void StopAndClear()
        {
            Active = false;
            Body = null;
            MasterTask?.AbortTask();
            CurrentTask?.AbortTask();
            MasterTask = null;
            CurrentTask = null;
        }

        void OnTaskCompleted()
        {
            CurrentTask?.Complete();
        }
    }
}
