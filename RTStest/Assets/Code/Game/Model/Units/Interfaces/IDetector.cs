﻿using System.Collections.Generic;
using UniRx;

namespace Code.Game.Model.Units.Interfaces {
    public interface IDetector : ITransformable
    {
        //from 0 to 100
        Dictionary<DetectChannel, int> DetectorCoef { get; }
        Dictionary<DetectChannel, float> DetectRange { get; }
        IReadOnlyReactiveProperty<bool> CanDetect { get; }

        void EnableDetector();
        void DisableDetector();
    }
}
