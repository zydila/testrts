﻿using System.Collections.Generic;
using Code.Game.Model.Units.Interfaces;

namespace Code.Game.Model.Units.Interfaces
{
    public interface IUnitHolder
    {
        List<IUnit> UnitsInside { get; }
        bool HaveSpace { get; }

        bool BoardUnit(IUnit unit);
        bool UnboardUnit(IUnit unit);
    }
}