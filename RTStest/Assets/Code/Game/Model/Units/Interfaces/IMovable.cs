﻿using Code.Game.Model;
using Code.Game.Model.Units.Interfaces;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

namespace Code.Game.Model.Units {
    public interface IMovable : ITransformable
    {
        float Velocity { get; set; }
        float MaxVelocity { get; }
        float Acceleration { get; }
        float RotationSpeed { get; }
        UnitElement Element { get; }
        IReadOnlyReactiveProperty<bool> CanMove { get; }
        IReadOnlyReactiveProperty<MovingMode> MovingMode { get; }

        void SetNewPosition(Vector3 position);
        void SetNewRotation(Quaternion rotation);
    }
}
