﻿using Code.Game.Model.Units.Tasks;

namespace Code.Game.Model.Units.Interfaces {
    public interface IAIControlable
    {
        UnitAI AI { get; }

        void AddAI(UnitAI ai);
        void ExecuteTask(TaskBase task);
        void StopTask();
    }
}
