﻿using System.Collections.Generic;
using UniRx;

namespace Code.Game.Model.Units.Interfaces {
    public interface IDetectable : ITransformable
    {
        //from 0 to 100
        Dictionary<DetectChannel, int> DetectableCoef { get; }
        IReadOnlyReactiveProperty<bool> IsDetected { get; }
        
        int LastDetectedFrame { get; } 
        float LastDetectedTime { get; } 
        int Hidden { get; }

        void Detect();
        void Forget();
    }
}
