﻿using System.Collections.Generic;
using Code.Game.Model.Units.Weapons;
using UniRx;

namespace Code.Game.Model.Units.Interfaces {
    public interface IDamageable
    {
        IReadOnlyReactiveProperty<DamageSeverity> DamageSeverity { get; }
        IReadOnlyReactiveProperty<bool> Destroyed { get; }
        Armor Armor { get; }

        void AddArmor(Armor armor);
        void ApplyDamage(DamageType type, int power);
        void Kill();
    }
}
