﻿using Code.Game.Model;
using UniRx;
using UnityEngine;

namespace Code.Game.Model.Units.Interfaces {
    public interface ITransformable
    {
        IReadOnlyReactiveProperty<Vector3> Position { get; }
        //Vector3 Position { get; }
        IReadOnlyReactiveProperty<Quaternion> Rotation { get; }
        IReadOnlyReactiveProperty<Vector3> Scale { get; }
        IReadOnlyReactiveProperty<Square> CurrentSquare { get; }

        void SetCurrentSquare(Square square);
    }
}
