﻿namespace Code.Game.Model.Units {
    public interface ISceneSelectable : ISceneClickable
    {
        void Select();
        void Deselect();
        
        bool IsSelected { get; }
    }
}
