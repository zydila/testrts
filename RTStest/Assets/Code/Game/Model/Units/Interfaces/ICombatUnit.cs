﻿using System.Collections.Generic;
using Code.Game.Model.Units.Interfaces;
using Code.Game.Model.Units.Weapons;
using UniRx;

namespace Code.Game.Model.Units {
    public interface ICombatUnit : IUnit
    {
        IReadOnlyReactiveProperty<bool> CanShoot { get; }
        IReadOnlyReactiveProperty<bool> PreparedForShooting { get; }

        List<WeaponBase> Weapons { get; }
        WeaponBase SelectedWeapon { get; }
        float PreparationTime { get; }

        void EquipWeapon(WeaponBase weapon);
        void RemoveWeapon(WeaponBase weapon);
        void SetPreparedForShooting(bool prepared);
    }
}
