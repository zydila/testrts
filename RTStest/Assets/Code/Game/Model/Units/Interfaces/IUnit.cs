﻿using System.Collections.Generic;
using UniRx;

namespace Code.Game.Model.Units.Interfaces {
    public interface IUnit : IDamageable, IDetector, IDetectable
    {
        string DesiredBody { get; }
        UnitType Type { get; }
        Faction Faction { get; }
        List<string> Effects { get; }
        //from 1 to 100
        int Experience { get; }
        //from -100 to 100
        int Moral { get; }
        ReactiveCommand Destroy { get; }
    }
}
