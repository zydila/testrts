﻿namespace Code.Game.Model.Units {
    public interface ISceneClickable
    {
        void OnClicked();
        bool CheckLayer(int layer);
    }
}
