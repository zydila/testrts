﻿namespace Code.Game.Model.Units {
    public enum UnitType
    {
        None,
        Civilian,
        Military,
        Unknown,
    }
}
