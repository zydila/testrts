﻿using System.Collections.Generic;

namespace Code.Game.Model.Units.Effects {
    public static class GlobalEffectsContainer
    {
        public readonly static Dictionary<string, EffectBase> Effects = new  Dictionary<string, EffectBase>();

        public static void AddEffect(EffectBase effect)
        {
            if (Effects.ContainsKey(effect.Tag))
                Effects[effect.Tag] = effect;
            else
                Effects.Add(effect.Tag, effect);
        }
    }
}
