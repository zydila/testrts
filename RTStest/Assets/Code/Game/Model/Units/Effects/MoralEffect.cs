﻿using System;

namespace Code.Game.Model.Units.Effects {
    public class MoralEffect : EffectBase
    {
        public override string Name
        {
            get
            {
                return "Moral Effect";
            }
        }

        public int MoralDelta { get; }

        public MoralEffect(int delta, string tag) : base (tag)
        {
            MoralDelta = delta;
        }
        
        public MoralEffect(int delta, string tag, int timeActive, Action onEnd) : base (tag, timeActive, onEnd)
        {
            MoralDelta = delta;
        }

    }
}
