﻿using System;
using Code.Core;
using Code.Core.Managers;

namespace Code.Game.Model.Units.Effects {
    public abstract class EffectBase
    {
        [Inject] TimerManager m_TimerManager;
        
        public abstract string Name { get; }
        public string Tag { get; }

        readonly Timer m_Timer;

        protected EffectBase(string tag)
        {
            Tag = tag;
        }
        
        protected EffectBase(string tag, int timeActive, Action onEffectExpire)
        {
            this.Inject();
            Tag = tag;
            m_Timer = m_TimerManager.StartTimer(timeActive, false, onEffectExpire);
        }

        public void ExtendEffect(int extendTime)
        {
            if (m_Timer == null)
            {
                Log.Warning("Timer not set for this event");
                return;
            }
            m_Timer.ExtendTimer(extendTime);
        }
    }
}
