﻿using System;
using Code.Core;
using Code.Core.Managers;
using Code.Game.Model.Services;
using Code.Game.Model.Units.Interfaces;

namespace Code.Game.Model.Units {
    public class UnitBuilder<T> where T : IUnit, new()
    {
        [Inject] UnitsManager m_UnitsManager;
        [Inject] MapManager m_MapManager;
        
        UnitDetails m_UnitDetails;
        T unit;
        
        public UnitBuilder<T> For(UnitDetails details)
        {
            m_UnitDetails = details;
            unit = new T();
            m_UnitsManager.AddUnit(unit);
            m_MapManager.MapGrid.AddUnitToGrid(unit);
            m_UnitsManager.UnitBodiesPresenter.AddUnit(unit);
            return this;
        }

        public UnitBuilder<T> WithEnemyDetector()//DetectorService detector)
        {
            if (unit is IDetector)
            {
            }
            else
            {
                Log.Warning("Unit doesn't support detection");
            }
            return this;
        }
        
        public UnitBuilder<T> WithMover(UnitMover mover)
        {
            if (unit is IMovable)
            {
            }
            else
            {
                Log.Warning("Unit doesn't support movement");
            }
            return this;
        }
        
        public UnitBuilder<T> WithCombater()//UnitCombatService unitCombat)
        {
            if (unit is ICombatUnit)
            {
            }
            else
            {
                Log.Warning("Unit doesn't support Combat");
            }
            return this;
        }

        public T Build()
        {
            return unit;
        }
    }
}
