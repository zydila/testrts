﻿using System.Collections.Generic;
using Code.Game.Model.Units.Weapons;

namespace Code.Game.Model.Units {
    public class Armor
    {
        //Type, power from 1 to 99
        public readonly Dictionary<DamageType, int> DamageProtect = new Dictionary<DamageType, int>();

        public Armor(Dictionary<DamageType, int> damageProtect)
        {
            DamageProtect = damageProtect;
        }
    }
}
