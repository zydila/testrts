﻿namespace Code.Game.Model.Units
{
    public enum MovingMode
    {
        Idle,
        Walking,
        Running,
        Crouching,
        Crawling
    }
}
