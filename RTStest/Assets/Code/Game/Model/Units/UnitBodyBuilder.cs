﻿
using Code.Core.Managers;
using Code.Game.Model.Units.Interfaces;
using UnityEngine;

namespace Code.Game.Model.Units {
    public class UnitBodyBuilder<T> where T : UnitBody, new()
    {
        [Inject] protected UnitsManager m_UnitsManager;

        UnitBody body;
        IUnit unit;
        UnitAI ai;

        public UnitBodyBuilder()
        {
            this.Inject();
        }
        
        public virtual UnitBodyBuilder<T> For(IUnit unit)
        {
            this.unit = unit;
            body = m_UnitsManager.GetUnitBody(unit.DesiredBody);
            return this;
        }

        public UnitBodyBuilder<T> AtLocation(Vector3 location)
        {
            body.transform.localPosition = location;
            return this;
        }

        public UnitBodyBuilder<T> WithRotation(Quaternion rotation)
        {
            body.transform.rotation = rotation;
            return this;
        }

        public T Build()
        {
            body.Init(unit);
            body.Enable();
            return body as T;
        }

        public virtual void Clean()
        {
            body = null;
            ai = null;
            unit = null;
        }
    }
}
