﻿namespace Code.Game.Model.Units
{
    public enum UnitElement
    {
        All,
        Ground,
        Water,
        Air,
        Amphibious
    }
}