﻿using System;
using System.Collections.Generic;
using Code.Core;
using Code.Game.Model.Units.Interfaces;
using Code.Game.Model.Units.Weapons;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Code.Game.Model.Units {
    public class UnitObservable : ICombatUnit, IMovable, IDisposable
    {
        public IReadOnlyReactiveProperty<Vector3> Scale => m_Scale;
        public IReadOnlyReactiveProperty<bool> CanMove => m_CanMove;
        public IReadOnlyReactiveProperty<bool> CanDetect => m_CanDetect;
        public IReadOnlyReactiveProperty<bool> IsDetected => m_IsDetected;
        public IReadOnlyReactiveProperty<Vector3> Position => m_Position;
        public IReadOnlyReactiveProperty<Quaternion> Rotation  => m_Rotation;
        public IReadOnlyReactiveProperty<MovingMode> MovingMode => m_MovingMode;
        public IReadOnlyReactiveProperty<Square> CurrentSquare => m_CurrentSquare;
        public IReadOnlyReactiveProperty<DamageSeverity> DamageSeverity => m_DamageSeverity;
        public IReadOnlyReactiveProperty<bool> Destroyed => m_Destroyed;

        public ReactiveCommand Destroy { get; } = new ReactiveCommand();
        
        public int Moral { get; }
        public UnitType Type { get; }
        public int Experience { get; }
        public Faction Faction { get; set; }
        public float MaxVelocity { get; } = 15f;
        public float Acceleration { get; } = 3f;
        public string DesiredBody { get; } = "T64";
        public UnitElement Element { get; } = UnitElement.Ground;
        public float RotationSpeed { get; } = 60f;
        public List<string> Effects { get; }
        public float Velocity { get; set;  }
        public Armor Armor { get; private set; }
        public int LastDetectedFrame { get; private set; }
        public float LastDetectedTime { get; private set; }
        public int Hidden { get; } = 0;

        public Dictionary<DetectChannel, int> DetectorCoef { get; } = new Dictionary<DetectChannel, int>
        {
            { DetectChannel.Visibility, 99 }, //Experience
        };

        public Dictionary<DetectChannel, float> DetectRange { get; } = new Dictionary<DetectChannel, float>
        {
            { DetectChannel.Visibility, 150 }, //Experience
        };

        public Dictionary<DetectChannel, int> DetectableCoef { get; } = new Dictionary<DetectChannel, int>
        {
            { DetectChannel.Visibility, 99 }, //Experience
        };

        readonly ReactiveProperty<Vector3> m_Scale = new Vector3ReactiveProperty();
        readonly ReactiveProperty<bool> m_IsDetected = new ReactiveProperty<bool>();
        readonly ReactiveProperty<Vector3> m_Position = new ReactiveProperty<Vector3>();
        readonly ReactiveProperty<Square> m_CurrentSquare = new ReactiveProperty<Square>();
        readonly ReactiveProperty<Quaternion> m_Rotation  = new ReactiveProperty<Quaternion>();
        readonly ReactiveProperty<bool> m_CanMove = new ReactiveProperty<bool>(true);
        readonly ReactiveProperty<MovingMode> m_MovingMode = new ReactiveProperty<MovingMode>();
        readonly ReactiveProperty<bool> m_CanDetect = new ReactiveProperty<bool>(true);
        readonly ReactiveProperty<DamageSeverity> m_DamageSeverity = new ReactiveProperty<DamageSeverity>(Units.DamageSeverity.NoDamage);
        readonly ReactiveProperty<bool> m_Destroyed = new ReactiveProperty<bool>(false);
        
        public UnitObservable()
        {
            Destroy.DoOnCompleted(() => Log.Warning("DESTROY unit not implemented")); //TODO I don't know if it works
            Faction = Random.value > 0.5f ? Faction.Blue : Faction.Red;
        }
        
        public void SetCurrentSquare(Square square)
        {
            if (CurrentSquare.Value == square || square == null)
                return;
            CurrentSquare.Value?.RemoveUnit(this);
            m_CurrentSquare.Value = square;
            square.AddUnit(this);
        }
        
        public void SetNewPosition(Vector3 newPosition)
        {
            m_Position.Value = newPosition;
        }

        public void SetNewRotation(Quaternion newRotation)
        {
            m_Rotation.Value = newRotation;
        }
        
        public void EnableDetector()
        {
            m_CanDetect.Value = true;
        }

        public void DisableDetector()
        {
            m_CanDetect.Value = false;
        }

        public void AddArmor(Armor armor)
        {
            Armor = armor;
        }

        public void ApplyDamage(DamageType type, int power)
        {
            if (Armor == null)
            {
                Kill();
                return;
            }
            if (!Armor.DamageProtect.ContainsKey(type))
                return;
            if (power < Armor.DamageProtect[type])
                return;
            Kill();
        }

        public virtual void Kill()
        {
            CurrentSquare.Value?.RemoveUnit(this);
            m_DamageSeverity.Value = Units.DamageSeverity.Destroyed;
            m_CanMove.Value = false;
            Log.Info($"Unity Killed");
        }
        
        public void Detect()
        {
            m_IsDetected.Value = true;
            LastDetectedFrame = Time.frameCount;
            LastDetectedTime = Time.timeSinceLevelLoad;
            //Log.Info($"Unit detected {DesiredBody}");//TODO change log
        }

        public void Forget()
        {
            m_IsDetected.Value = false;
            //Log.Info($"Unit forgotten {DesiredBody}");//TODO change log
        }

        public void Dispose()
        {
            
        }

        public IReadOnlyReactiveProperty<bool> CanShoot { get; }
        public IReadOnlyReactiveProperty<bool> PreparedForShooting => m_PreparedForShooting;
        public ReactiveProperty<bool> m_PreparedForShooting = new ReactiveProperty<bool>();
        public List<WeaponBase> Weapons { get; } = new List<WeaponBase>();
        public WeaponBase SelectedWeapon { get; private set; }
        public float PreparationTime { get; } = 3f;

        public void EquipWeapon(WeaponBase weapon)
        {
            Weapons.Add(weapon);
            SelectedWeapon = weapon;
        }

        public void RemoveWeapon(WeaponBase weapon)
        {
            Weapons.Remove(weapon);
        }

        public void SetPreparedForShooting(bool prepared)
        {
            m_PreparedForShooting.Value = prepared;
        }
    }
}
