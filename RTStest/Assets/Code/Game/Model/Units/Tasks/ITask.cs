﻿using System;

namespace Code.Game.Model.Units.Tasks {
    public interface ITask<out T> : IObservable <T>
    {
        TaskPriority Priority { get; }
        string Description { get; }
        bool InProgress { get; }
        bool Completed { get; }

        void Start();
        void Complete();
    }
}
