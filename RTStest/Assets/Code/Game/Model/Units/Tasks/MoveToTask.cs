﻿using UnityEngine;

namespace Code.Game.Model.Units.Tasks {
    public class MoveToTask : TaskBase
    {
        public override string Description
        {
            get { return $"Move To {Destination} Task"; }
        }
        
        public Vector3 Destination { get; }
        public MoveTaskFilter Filter { get; }
        Transform m_ControlledBodyTransform;

        public float DestinationRadius { get; private set; }

        public MoveToTask(TaskPriority priority, Vector3 destination, MoveTaskFilter filter, Transform controlledBodyTransform) : base(priority)
        {
            m_ControlledBodyTransform = controlledBodyTransform;
            Destination = destination;
            Filter = filter;
            DestinationRadius = 2f;
        }

        public void SetDestinationAllowanceRadius(float radius)
        {
            DestinationRadius = radius;
        }

        public override bool CompleteConditionCheck()
        {
            float distance = Mathf.Abs((m_ControlledBodyTransform.position - Destination).magnitude);
            return distance < DestinationRadius;
        }
    }
}
