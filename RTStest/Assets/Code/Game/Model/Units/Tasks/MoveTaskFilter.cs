﻿namespace Code.Game.Model.Units.Tasks {
    public enum MoveTaskFilter
    {
        None,
        Fastest,
        Safest
    }
}
