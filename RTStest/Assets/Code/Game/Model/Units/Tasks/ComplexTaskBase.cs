﻿using System;
using System.Collections.Generic;

namespace Code.Game.Model.Units.Tasks {
    public abstract class ComplexTaskBase : TaskBase
    {
        protected readonly Queue<TaskBase> Subtasks = new Queue<TaskBase>();
        public TaskBase m_CurrentTask;

        public event Action SubtaskCompleted;
    
        public ComplexTaskBase(TaskPriority priority) : base(priority) { }

        protected override void StartInternal()
        {
        }

        public override TaskBase GetActualTask()
        {
            if (Subtasks.Count == 0)
                return null;
            PeekTask();
            return m_CurrentTask;
        }

        void PeekTask()
        {
            if (m_CurrentTask == Subtasks.Peek())
                return;
            m_CurrentTask = Subtasks.Peek();
            if (m_CurrentTask == null)
                return;
            m_CurrentTask.OnCompleted += OnSubtaskCompleted;
            m_CurrentTask.OnAborted += OnSubtaskCompleted;
        }

        void OnSubtaskCompleted()
        {
            Subtasks.Dequeue();
//            if (Subtasks.Count == 0)
//                Complete();
            StartInternal();
            SubtaskCompleted?.Invoke();
        }

        protected override void StopTaskInternal()
        {
            m_CurrentTask?.StopTask();
        }
    }
}
