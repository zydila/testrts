﻿namespace Code.Game.Model.Units.Tasks {
    public enum TaskPriority
    {
        Low,
        Medium,
        High
    }
}
