﻿using System;
using System.Collections.Generic;
using Code.Core;

namespace Code.Game.Model.Units.Tasks {
    public abstract class TaskBase
    {
        public abstract string Description { get; }
        
        public event Action OnStarted;
        public event Action OnCompleted;
        public event Action OnAborted;
        
        public bool InProgress { get; private set; }
        public bool Completed { get; private set; }
        public TaskPriority Priority { get; private set; }

        protected TaskBase(TaskPriority priority)
        {
            Priority = priority;
        }
        
        public void Start()
        {
            Completed = false;
            InProgress = true;
            StartInternal();
            OnStarted?.Invoke();
        }

        public void Complete()
        {
            InProgress = false;
            Completed = true;
            CompleteInternal();
            OnCompleted?.Invoke();
        }

        public virtual TaskBase GetActualTask()
        {
            return this;
        }

        public void AbortTask()
        {
            Completed = false;
            InProgress = false;
            OnAborted?.Invoke();
        }

        public void StopTask()
        {
            StopTaskInternal();
            Complete();
        }

        public virtual bool TaskCanBeDone(UnitDetailsOld detailsOld)
        {
            //Log.Info($"Task {Description}, Condition passed");
            return true;
        }

        public abstract bool CompleteConditionCheck();
        
        protected virtual void StopTaskInternal(){ }
        protected virtual void StartInternal() { }
        protected virtual void CompleteInternal() { }
    }
}
