﻿using System;
using System.Collections.Generic;
using Code.Core;
using Code.Core.Common;

namespace Code.Game.Model.Units.Tasks {
    public abstract class TaskBase2<T> : ITask<T>
    {
        public abstract string Description { get; }
        public TaskPriority Priority { get; }
        public bool InProgress { get; private set; }
        public bool Completed { get; private set; }
        
        readonly List<IObserver<T>> m_Observers = new List<IObserver<T>>();

        public TaskBase2(TaskPriority priority)
        {
            Priority = priority;
        }
        
        public IDisposable Subscribe(IObserver<T> observer)
        {
            m_Observers.Add(observer);
            return new EmptyDisposable();
        }

        public void Start()
        {
            //Log.Info($"Task {Description} started");
            InProgress = true;
            StartInternal();
        }

        public void Complete()
        {
            //Log.Info($"Task {Description} completed");
            InProgress = false;
            Completed = true;
            CompleteInternal();
            NotifyCompleted();
        }
        protected virtual void StartInternal() { }
        protected virtual void CompleteInternal() { }

        public void Error<TE>(TE exception) where TE : Exception
        {
            Log.Warning($"Task {Description} cannot be completed");
            NotifyError(exception);
        }
        
        protected void NotifyNext(T value)
        {
            foreach (IObserver<T> observer in m_Observers)
                observer.OnNext(value);
        }

        void NotifyCompleted()
        {
            foreach (IObserver<T> observer in m_Observers)
                observer.OnCompleted();
        }

        protected void NotifyError<TE>(TE exception) where TE : Exception
        {
            foreach (IObserver<T> observer in m_Observers)
                observer.OnError(exception);
        }
    }
}
