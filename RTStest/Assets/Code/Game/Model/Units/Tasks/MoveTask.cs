﻿using System;
using Code.Core;
using UnityEngine;
using UnityEngine.AI;

namespace Code.Game.Model.Units.Tasks {
    public class MoveTask : TaskBase2<ITask<Vector3>>
    {
        public Vector3 Destination { get; }
        public MovingMode Mode { get; }
        
        public override string Description { get { return "MoveTask to " + Destination; } }
        
        public MoveTask(TaskPriority priority, MovingMode mode, Vector3 destination) : base(priority)
        {
//            if (NavMesh.SamplePosition(destination, out NavMeshHit navMeshHit, 10f, NavMesh.AllAreas))
//                Destination = navMeshHit.position;
//            else
                Destination = destination;
            Mode = mode;
        }

    }
}
