﻿using UnityEngine;

namespace Code.Game.Model.Units.Tasks {
    public class ComplexMoveTask : ComplexTaskBase
    {
        public override string Description { get { return "ComplexMoveTask"; } }

        Vector3[] m_Destinations;
        
        public ComplexMoveTask(TaskPriority priority, Vector3[] destinations, Transform controlledBody) : base(priority)
        {
            m_Destinations = destinations;
            for (int i = 0; i < destinations.Length; i++)
                Subtasks.Enqueue(new MoveToTask(priority, destinations[i], MoveTaskFilter.Fastest, controlledBody));
        }
        
        public override bool CompleteConditionCheck()
        {
            return Subtasks.Count == 0;
        }
    }
}
