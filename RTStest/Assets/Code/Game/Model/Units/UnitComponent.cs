﻿using UnityEngine;

namespace Code.Game.Model.Units {
    public abstract class UnitComponent : MonoBehaviour
    {
        public abstract void StopAndClear();
    }
}
