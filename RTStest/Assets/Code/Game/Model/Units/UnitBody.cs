﻿using System;
using System.Collections.Generic;
using Code.Core;
using Code.Core.Managers;
using Code.Game.Model.Units.Interfaces;
using UniRx;
using UnityEngine;

namespace Code.Game.Model.Units {
    public class UnitBody : MonoBehaviour, ISceneSelectable
    {
        [Inject] UnitsManager m_UnitsManager;
        
        [SerializeField] Transform effectsRoot;
        [SerializeField] GameObject componentsRoot;
        [SerializeField] protected Rigidbody rigibody;
        
        public string BodyName { get; private set; }
        public bool Inited { get; private set; }

        IDisposable m_MoveSub;
        IDisposable m_RotationSub;
        
        public void Init()
        {
            this.Inject();
            if (Inited)
                return;
            this.Inject();
            if (rigibody == null)
                rigibody = GetComponent<Rigidbody>();

            if (effectsRoot == null)
                effectsRoot = GameObject.Find("Effects")?.GetComponent<Transform>();
            if (effectsRoot == null)
                effectsRoot = gameObject.transform;
            
            if (componentsRoot == null)
            {
                componentsRoot = new GameObject("Components");
                componentsRoot.transform.parent = transform;
                componentsRoot.transform.localPosition = Vector3.zero;
            }

            if (name.Contains("(Clone)"))
                BodyName = name.Replace("(Clone)", "");
            if (name.Contains("["))
                BodyName = name.Split('-')[0];
            if (name.Contains("(Clone)"))
                name = BodyName + $"-[{UnitsManager.UnitsCounter.ToString()}]";
            InitInternal();
            Inited = true;
        }
        
        protected virtual void InitInternal() { }
        
        public void Init(IUnit unit)
        {
            Init();
            ApplyEffects(unit.Effects);
            m_MoveSub = unit.Position.Subscribe((position) => transform.position = position);
            m_RotationSub = unit.Rotation.Subscribe((rotation) => transform.rotation = rotation);
        }
        
        public void Disable()
        {
            gameObject.SetActive(false);
            DisableRigidbody();
        }

        public void Enable()
        {
            gameObject.SetActive(true);
            EnableRigidbody();
        }

        public virtual void ResetState()
        {
            //ClearEffects();
            ResetTransform();
            //ResetRigidbody();
            //ResetAnimations();
            //ClearComponents();
            //componentsRoot = null;
            //effectsRoot = null;
            Inited = false;
        }

        void DisposeSubs()
        {
            m_MoveSub?.Dispose();
            m_RotationSub.Dispose();
        }

        void ResetTransform()
        {
            //transform.position = Vector3.zero;;
            transform.rotation = new Quaternion();
            transform.localScale = Vector3.one;
        }

        readonly List<string> effects = new List<string>(); // placeHolder
        public void ApplyEffects(List<string> effects)
        {
            if (effects == null)
                return;
            foreach (string effect in effects)
                ApplyEffect(effect);
        }
        
        public void ApplyEffect(string effect)
        {
            effects.Add(effect);
        }

        protected virtual void ResetAnimations()
        {
            Log.Warning("Clear animations");
        }

        void ClearEffects()
        {
            if (effectsRoot == null)
                return;
            effects.Clear();
            for (int i = 0; i < effectsRoot.childCount; i++)
                GameObject.Destroy(effectsRoot.GetChild(i).gameObject);
        }

        void ResetRigidbody()
        {
            if (rigibody == null)
                return;
            rigibody.freezeRotation = false;
            rigibody.velocity = Vector3.zero;
            rigibody.position =  Vector3.zero;
            rigibody.MoveRotation(new Quaternion().normalized);
            rigibody.ResetInertiaTensor();
            rigibody.ResetCenterOfMass();
        }

        void EnableRigidbody()
        {
            if (rigibody == null)
                return;
            //rigibody.isKinematic = true;
            rigibody.detectCollisions = true;
        }

        void DisableRigidbody()
        {
            if (rigibody == null)
                return;
            //rigibody.isKinematic = false;
            rigibody.detectCollisions = false;
        }

        void ClearComponents()
        {
            if (componentsRoot == null)
                return;
            for (int i = 0; i < componentsRoot.transform.childCount; i++)
            {
                UnitComponent unitComponent = componentsRoot.transform.GetChild(i).GetComponent<UnitComponent>();
                unitComponent.StopAndClear();
            }
            foreach (Transform child in componentsRoot.transform)
                GameObject.Destroy(child.gameObject);
        }

        public void OnClicked()
        {
            Log.Info("Clicked On " + gameObject.name);
        }

        public void Select()
        {
            throw new NotImplementedException();
        }

        public void Deselect()
        {
            throw new NotImplementedException();
        }

        public bool IsSelected { get; }

        public bool CheckLayer(int layer)
        {
            return gameObject.layer == layer;
        }

        public void Destroy()
        {
            this.Inject();
            m_MoveSub?.Dispose();
            m_RotationSub?.Dispose();
            if (!m_UnitsManager.AddBodyToPool(this))
            {
                ResetState();
                Destroy(this);
            }
        }
    }
}
