﻿using Code.Core;
using Code.Game.Model.Units.Interfaces;
using Code.Game.Model.Units.Tasks;

namespace Code.Game.Model.Units {
    public abstract class UnitWithAI : UnitBase, IAIControlable
    {
        public UnitAI AI { get; private set; }
        
        public override void Print()
        {
            Log.Info("AI UNIT");
        }
    
        public void AddAI(UnitAI ai)
        {
            AI = ai;
        }

        public void ExecuteTask(TaskBase task)
        {
            if (AI == null)
            {
                Log.Error("Task cannot be completed without AI");
                return;
            }
            AI.ExecuteTask(task);
        }

        public void StopTask()
        {
            AI.StopTask();
        }

        public override void ResetUnit()
        {
            AI.StopAndClear();
            base.ResetUnit();
        }
    }
}