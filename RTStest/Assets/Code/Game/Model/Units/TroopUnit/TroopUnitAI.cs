﻿using System;
using Code.Core;
using Code.Game.Model.Units.Tasks;
using UnityEngine;

namespace Code.Game.Model.Units
{
    public class TroopUnitAI : UnitAI
    {
        [SerializeField] UnityEngine.AI.NavMeshAgent agent;
        Vector3 m_TargetDestination;
        
        bool m_TargetSet;
        
        protected override void InitInternal()
        {
            //agent = (Body as IMovable)?.NavMeshAgent;
            agent = (Body as TroopUnitBody)?.NavMeshAgent;
            if (agent == null)
                throw new Exception("TroopAI require Body to be Movable");
            agent.updateRotation = false;
            agent.updatePosition = true;
        }

        protected override void ExecuteCurrentTask()
        {
            ProcessMoveTask();
        }

        protected override void CheckEnvironment()
        {
            
        }

        void ProcessMoveTask()
        {
            if (!m_TargetSet && (CurrentTask is MoveToTask moveTask))
            {
                if (!CurrentTask.InProgress)
                    CurrentTask.Start();
                CurrentTask.OnCompleted += StopMovement;
                m_TargetDestination = moveTask.Destination;
                m_TargetSet = true;
                agent.stoppingDistance = moveTask.DestinationRadius;
                agent.SetDestination(m_TargetDestination);
            }
            if (!m_TargetSet)
                return; 
            //(Body as IMovable)?.Move(agent.desiredVelocity, false, false);
            (Body as TroopUnitBody)?.Move(agent.desiredVelocity, false, false);
        }

        void StopMovement()
        {
            m_TargetSet = false; 
            //(Body as IMovable)?.StopMovement();
            (Body as TroopUnitBody)?.StopMovement();
        }
    }
}
