﻿namespace Code.Game.Model.Units {
    public class TroopUnitDetailsOld : UnitDetailsOld
    {
        public override string UnitName { get { return "Troop Unit"; } }
        public override string DesiredBody { get { return "TroopUnit"; } }
    }
}
