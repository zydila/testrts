﻿using Code.Core;
using Code.Core.Managers;
using Code.Game.Model.Units.Tasks;

namespace Code.Game.Model.Units {
    public abstract class UnitBase
    {
        [Inject] UnitsManager m_UnitsManager;
        
        public UnitBody Body { get; private set; }
        public UnitDetailsOld DetailsOld { get; private set; }
        
        protected TaskBase m_Task;

        public void Init(UnitDetailsOld detailsOld)
        {
            this.Inject();
            DetailsOld = detailsOld;
        }
    
        public void AddBody(UnitBody body)
        {
            Body = body;
        }

        public virtual void Print()
        {
            Log.Info("UnitBase");
        }

        public virtual void ResetUnit()
        {
            Body?.ResetState();
        }

        public virtual void DamageReceived()
        {
            DetailsOld.DamageSeverity++;
        }

        public void DestroyUnit()
        {
            ResetUnit();
            m_UnitsManager.AddBodyToPool(Body);
        }
    }
}
