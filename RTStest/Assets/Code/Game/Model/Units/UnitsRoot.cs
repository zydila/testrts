﻿using System.Collections.Generic;
using Code.Core.Managers;
using UnityEngine;

namespace Code.Game.Model.Units {
    public class UnitsRoot : MonoBehaviour
    {
        [Inject] UnitsManager m_UnitsManager;

        public List<UnitBody> UnitBodies { get; private set; } = new List<UnitBody>();
        
        void Awake()
        {
            this.Inject();
            m_UnitsManager.SetUnitRoot(this);
        }

        public void Add(UnitBody body)
        {
            UnitBodies.Add(body);
            body.transform.parent = transform;
        }

        public void Remove(UnitBody body)
        {
            UnitBodies.Remove(body);
        }

        public void ClearRoot()
        {
            UnitBodies.Clear();
        }
        
        public bool TryCopyExistingBody(string bodyName, out UnitBody resultBody)
        {
            foreach (UnitBody unitBody in UnitBodies)
            {
                if (unitBody.BodyName == bodyName)
                {
                    resultBody = Instantiate(unitBody);
                    UnitsManager.UnitsCounter++;
                    resultBody.ResetState();
                    resultBody.Disable();
                    return true;
                }
            }
            resultBody = null;
            return false;
        }
    }
}
