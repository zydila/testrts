﻿using System.Collections.Generic;
using System.Linq;
using Code.Core;
using Code.Core.Managers;
using UnityEngine;

namespace Code.Game.Model.Units {
    public class UnitsPool : MonoBehaviour
    {
        [Inject] UnitsManager m_UnitsManager;
        [Inject] TimerManager m_TimerManager;

        const int MAX_POOL_SIZE = 100;
        const int UNUSED_TIME_LIMIT_IN_SEC = 120;
        const int UNUSED_CHECK_TIME_IN_SEC = 30;

        readonly List<BodyInfo> m_BodiesPool = new List<BodyInfo>();

        Timer m_Timer;

        void Awake()
        {
            this.Inject();
            m_UnitsManager.SetUnitsPool(this);
        }

        public bool Add(UnitBody body)
        {
            if (m_Timer == null)
                m_Timer = m_TimerManager.StartTimer(UNUSED_CHECK_TIME_IN_SEC, true, ClearUnusedFromPool);
            if (body == null)
            {
                Log.Warning("Trying to add null body to pool");
                return false;
            }

            if (m_BodiesPool.Count > MAX_POOL_SIZE)
            {
                Log.Warning($"Units Pool reached max size {MAX_POOL_SIZE}");
                return false;
            }
            
//            foreach (BodyInfo bodyInfo in m_BodiesPool)
//            {
//                if (bodyInfo.Body.BodyName == body.BodyName)
//                {
//                    Log.Warning("Body already in pool");
//                    bodyInfo.Timestamp = m_TimerManager.CurrentTimestamp;
//                    return false;
//                }
//            }
            
            body.Disable();
            body.ResetState();
            body.transform.parent = transform;
            m_BodiesPool.Add(new BodyInfo(body, m_TimerManager.CurrentTimestamp));
            return true;
        }

        public bool TryRetrieveBodyFromPool(string name, out UnitBody body)
        {
            for (var i = m_BodiesPool.Count - 1; i >= 0; i--)
            {
                if (m_BodiesPool[i].Body.BodyName == name)
                {
                    body = m_BodiesPool[i].Body;
                    m_BodiesPool.RemoveAt(i);
                    return true;
                }
                
            }
            foreach (BodyInfo bodyInfo in m_BodiesPool)
            {
            }
            body = null;
            return false;
        }

        void ClearUnusedFromPool()
        {
            for (int i = m_BodiesPool.Count - 1; i >= 0; i--)
            {
                if (m_TimerManager.CurrentTimestamp - m_BodiesPool[i].Timestamp > UNUSED_TIME_LIMIT_IN_SEC)
                {
                    m_BodiesPool[i].Body.Destroy();
                    m_BodiesPool.RemoveAt(i);
                }
            }
        }

        class BodyInfo
        {
            public readonly UnitBody Body;
            public long Timestamp;

            public BodyInfo(UnitBody body, long timestamp)
            {
                Body = body;
                Timestamp = timestamp;
            }
        }
    }
}
