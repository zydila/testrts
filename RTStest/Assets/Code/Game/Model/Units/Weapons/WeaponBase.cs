﻿using Code.Game.Model.Units;
using Code.Game.Model.Units.Interfaces;
using UniRx;
using UnityEngine;

namespace Code.Game.Model.Units.Weapons {
    public class WeaponBase
    {
        public IReadOnlyReactiveProperty<int> Amo => m_Amo;
        public IReadOnlyReactiveProperty<bool> IsFiring => m_IsFiring;
        
        public float RateOfFire { get; private set; } = 1f;
        public float MaxRange { get; private set; } = 300;
        public float EffectiveRange { get; private set; } = 150;
        public float MinRange { get; private set; } = 0;
        public int MaxAmo { get; private set; } = 100;

        public ICombatUnit m_Owner { get; private set; }
        public Projectile Projectile;
        
        ReactiveProperty<int> m_Amo = new ReactiveProperty<int>(-1);
        ReactiveProperty<bool> m_IsFiring = new ReactiveProperty<bool>(false);

        float lastShotTime;

        
        public virtual void FireAtLocation(Vector3 location) { }
        public virtual void FireAtUnit(IUnit unit) { }

        public void SetOwner(ICombatUnit owner)
        {
            m_Owner = owner;
        }
        
        public bool IfFiringPossible(Vector3 targetLocation)
        {
            float distance = (m_Owner.Position.Value - targetLocation).magnitude;
            return m_Amo.Value != 0 && distance < MaxRange && distance > MinRange && Time.timeSinceLevelLoad - lastShotTime > RateOfFire;
        }

        public void SetProjectile(Projectile projectile)
        {
            Projectile = projectile;
        }

        protected void DecreaseAmo(int decreaseAmount)
        {
            if (m_Amo.Value == -1)
                return;
            if (m_Amo.Value > 0)
                m_Amo.Value -= decreaseAmount;
            if (m_Amo.Value < 0)
                m_Amo.Value = 0;
        }

        protected void SupplyAmo(int addAmount)
        {
            if (m_Amo.Value == -1)
                return;
            m_Amo.Value += addAmount;
            if (m_Amo.Value > MaxAmo)
                m_Amo.Value = MaxAmo;
        }
    }
}
