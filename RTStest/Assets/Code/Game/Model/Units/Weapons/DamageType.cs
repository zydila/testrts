﻿namespace Code.Game.Model.Units.Weapons {
    public enum DamageType
    {
        Bullet,
        Cumulative,
        Shrapnel
    }
}
