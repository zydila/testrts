﻿using UniRx;
using UnityEngine;

namespace Code.Game.Model.Units.Weapons {
    public class Projectile
    {
        public IReadOnlyReactiveProperty<Vector3> Position => m_Position;
        public DamageType DamageType { get; private set; } = DamageType.Bullet;
        public int Power { get; private set; } = 100;
        public float Velocity { get; private set; } = 50f;

        ReactiveProperty<Vector3> m_Position = new ReactiveProperty<Vector3>();

        public void SetPosition(Vector3 newPos)
        {
            m_Position.Value = newPos;
        }
    }
}
