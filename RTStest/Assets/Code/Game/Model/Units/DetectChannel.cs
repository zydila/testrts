﻿namespace Code.Game.Model.Units {
    public enum DetectChannel
    {
        Visibility,
        AirRadar,
        ArtilleryRadar
    }
}
