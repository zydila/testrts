﻿using System.Collections.Generic;
using UnityEngine;

namespace Code.Game.Model.Units {
    public abstract class UnitDetailsOld
    {
        public abstract string DesiredBody { get; }
        public abstract string UnitName { get; }
        
        public UnitType Type { get; private set; } 
        public Faction Faction { get; private set; }
        public List<string> Effects { get; private set; }

        DamageSeverity m_DamageSeverity;
        public DamageSeverity DamageSeverity
        {
            get { return m_DamageSeverity; }
            set
            {
                if (value > m_DamageSeverity)
                    m_Moral -= 30;
                if (value < m_DamageSeverity)
                    m_Moral += 30;
                if (value == DamageSeverity.Critical)
                    m_Moral = Mathf.Clamp(0, -100, m_Moral);
                Experience += 1;
                m_DamageSeverity = value;
            }
        }

        int m_Experience;
        public int Experience
        {
            get { return m_Experience; }
            protected set { m_Experience = Mathf.Clamp(value, m_Experience, 100); }
        }
        
        int m_Moral;
        public int Moral
        {
            get { return m_Moral; }
            protected set { m_Moral = Mathf.Clamp(value, -100, 100); }
        } 
    }
}
