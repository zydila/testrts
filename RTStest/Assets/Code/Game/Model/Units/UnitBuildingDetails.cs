﻿using Code.Core;

namespace Code.Game.Model.Units {
    public class BuildingUnitDetailsOld : UnitDetailsOld
    {
        public override string DesiredBody { get { return "BasicBuilding"; } }
        public override string UnitName { get { return "cBuilding"; } }

        public virtual void Construct() { }
        public virtual void Destroy() { }
    }
}
