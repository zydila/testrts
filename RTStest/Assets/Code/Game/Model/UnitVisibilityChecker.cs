﻿using System.Collections.Generic;
using Code.Core;
using Code.Core.Managers;
using Code.Game.Model.Units.Interfaces;
using UnityEngine;

namespace Code.Game.Model
{
    public class UnitVisibilityChecker
    {
        [Inject] MapManager m_MapManager;
        
        RaycastHit m_HitInfo;
        Vector3 m_Direction;
        int m_VisibilityMask = (1 << GlobalConstants.TERRAIN_LAYER) | 
                               (1 << GlobalConstants.VISIBILITY_LAYER) |
                               (1 << GlobalConstants.WATER_LAYER);

        public UnitVisibilityChecker()
        {
            this.Inject();
        }
        
        public bool CheckVisibility(IDetector from, IDetectable to)
        {
//            Vector2Int[] squares = m_MapManager.MapGrid.GetSquaresAround(from.CurrentSquare.Value);
//            if (!squares.Contains(to.CurrentSquare.Value))
//                return false;

            m_Direction = to.Position.Value - from.Position.Value;
            return !Physics.Raycast(@from.Position.Value, m_Direction, out m_HitInfo, m_Direction.magnitude, m_VisibilityMask); //TODO think about when object has Body, raycast can collide on it
        }
    }
}