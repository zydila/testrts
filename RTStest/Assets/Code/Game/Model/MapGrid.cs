﻿using System;
using System.Collections.Generic;
using System.Linq;
using Code.Core;
using Code.Core.Managers;
using Code.Game.Model.Units.Interfaces;
using UniRx;
using UnityEngine;
using UnityEngine.Profiling;

namespace Code.Game.Model
{
    public class MapGrid
    {
        [Inject] EventDispatcher m_EventDispatcher;

        public const float SQUARE_SIZE = 90;

        public Square CurrentSquare
        {
            get { return GetSquare(m_CurrentSquare); }
        }

        Vector2Int[] m_ActiveSquares;
        Vector2Int[] m_SquaresCache;
        Square[,] m_UnitsSquares;
        Vector2Int m_CurrentSquare;

        int m_SquaresRows;
        int m_SquaresColumns;
        bool m_Inited;

        float m_StartX;
        float m_StartY;

        public MapGrid(Vector3 mapBound)
        {
            this.Inject();
            m_StartX = -mapBound.x / 2 + SQUARE_SIZE / 2;
            m_StartY = -mapBound.z / 2 + SQUARE_SIZE / 2;
            m_SquaresRows = Mathf.CeilToInt(mapBound.x / SQUARE_SIZE);
            m_SquaresColumns = Mathf.CeilToInt(mapBound.z / SQUARE_SIZE);
            m_UnitsSquares = new Square[m_SquaresRows, m_SquaresColumns];
            for (int i = 0; i < m_SquaresRows; i++)
                for (int j = 0; j < m_SquaresColumns; j++)
                    m_UnitsSquares[i, j] = new Square(new Vector2(m_StartX + SQUARE_SIZE * i, m_StartY + SQUARE_SIZE * j), SQUARE_SIZE, i, j);
            m_Inited = true;
            if (Debug.isDebugBuild)
                m_EventDispatcher.OnUpdate += Visualize;
        }
        
        public void AddUnitToGrid(IUnit unit)
        {
            unit.SetCurrentSquare(FindSquareForWorldPosition(unit.Position.Value));
            IDisposable subscriber = unit.Position.Subscribe(newPosition => unit.SetCurrentSquare(FindSquareForWorldPosition(newPosition)));
            unit.Destroy.DoOnCompleted(subscriber.Dispose);
        }

        public void OnPlayerChangedPosition(Vector3 value)
        {
            FindCurrentSquare(value);
            m_SquaresCache = GetSquaresAroundCurrent();
            if (m_ActiveSquares != null)
                for (int i = m_ActiveSquares.Length - 1; i >= 0; i--)
                {
                    if (m_SquaresCache != null)
                    {
                        bool found = false;
                        for (int j = 0; j < m_SquaresCache.Length; j++)
                        {
                            if (m_SquaresCache[j] == m_ActiveSquares[i])
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                            GetSquare(m_ActiveSquares[i]).Deactivate();
                    }
                    else
                        GetSquare(m_ActiveSquares[i]).Deactivate();
                }

            m_ActiveSquares = m_SquaresCache;
            if (m_ActiveSquares == null)
                return;
            foreach (Vector2Int square in m_ActiveSquares)
                GetSquare(square)?.Activate();
        }

        public Square FindSquareForWorldPosition(Vector3 position)
        {
            return GetSquare(FindSquareIndexForWorldPosition(position));
        }

        public Vector2Int FindSquareIndexForWorldPosition(Vector3 position)
        {
            float x = Mathf.Abs(m_StartX - position.x) / SQUARE_SIZE;
            float y = Mathf.Abs(m_StartY - position.z) / SQUARE_SIZE;
            return new Vector2Int(Mathf.Clamp(Mathf.RoundToInt(x), 0, m_SquaresRows), Mathf.Clamp(Mathf.RoundToInt(y), 0, m_SquaresColumns));
        }

        public Vector2Int FindSquareIndexForWorldPosition2(Vector3 position)
        {
            Vector2Int result = new Vector2Int(-1, -1);
            if (IsPointInSquare(m_CurrentSquare.x, m_CurrentSquare.y + 1, position))
            {
                result.Set(m_CurrentSquare.x, m_CurrentSquare.y + 1);
                return result;
            }
            if (IsPointInSquare(m_CurrentSquare.x, m_CurrentSquare.y - 1, position))
            {
                result.Set(m_CurrentSquare.x, m_CurrentSquare.y - 1);
                return result;
            }
            if (IsPointInSquare(m_CurrentSquare.x + 1, m_CurrentSquare.y, position))
            {
                result.Set(m_CurrentSquare.x + 1, m_CurrentSquare.y);
                return result;
            }
            if (IsPointInSquare(m_CurrentSquare.x + 1, m_CurrentSquare.y + 1, position))
            {
                result.Set(m_CurrentSquare.x + 1, m_CurrentSquare.y + 1);
                return result;
            }
            if (IsPointInSquare(m_CurrentSquare.x + 1, m_CurrentSquare.y - 1, position))
            {
                result.Set(m_CurrentSquare.x + 1, m_CurrentSquare.y - 1);
                return result;
            }
            if (IsPointInSquare(m_CurrentSquare.x - 1, m_CurrentSquare.y, position))
            {
                result.Set(m_CurrentSquare.x - 1, m_CurrentSquare.y);
                return result;
            }
            if (IsPointInSquare(m_CurrentSquare.x - 1, m_CurrentSquare.y + 1, position))
            {
                result.Set(m_CurrentSquare.x - 1, m_CurrentSquare.y + 1);
                return result;
            }
            if (IsPointInSquare(m_CurrentSquare.x - 1, m_CurrentSquare.y - 1, position))
            {
                result.Set(m_CurrentSquare.x - 1, m_CurrentSquare.y - 1);
                return result;
            }
            for (int i = 0; i < m_SquaresRows; i++)
            {
                for (int j = 0; j < m_SquaresColumns; j++)
                {
                    if (IsPointInSquare(i, j, position))
                    {
                        result.Set(i, j);
                        return result;
                    }
                }
            }
            throw new PlayerOutOfSquaresSpaceException();
        }

        public Vector2Int[] GetSquaresAroundCurrent()
        {
            return GetSquaresAround(m_CurrentSquare, 1);
        }

        public Vector2Int[] GetSquaresAround(Square square, int radius)
        {
            return GetSquaresAround(square.PositionInGrid, radius);
        }

        int squaresAroundArraySize;
        int index;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="square"></param>
        /// <param name="radius"></param>
        /// <returns></returns>
        public Vector2Int[] GetSquaresAround(Vector2Int square, int radius)
        {
            squaresAroundArraySize = (radius * 2 + 1) * (radius * 2 + 1);
            int halfSize = Mathf.FloorToInt(squaresAroundArraySize / 2) - radius;
            bool cutByX = false;
            if (square.x == 0 || square.x == m_SquaresRows - 1)
            {
                squaresAroundArraySize = squaresAroundArraySize - halfSize;
                cutByX = true;
            }
            if (square.y == 0 || square.y == m_SquaresColumns - 1)
                squaresAroundArraySize = squaresAroundArraySize - (cutByX ? Mathf.FloorToInt(halfSize / 2) : halfSize);
                
            Profiler.BeginSample("CreateVector");
            var result = new Vector2Int[squaresAroundArraySize];
            Profiler.EndSample();
            index = 0;
            for (int i = -radius; i <= radius; i++)
                for (int j = -radius; j <= radius; j++)
                    if (CheckSquareIndexes(square.x + i, square.y + j))
                    {
                        result[index] = GetSquare(square.x + i, square.y + j).PositionInGrid;
                        index++;
                    }
//            List<Square> result = new List<Square>();
//            for (int i = -1; i <= 1; i++)
//                for (int j = -1; j <= 1; j++)
//                    if (CheckSquareIndexes(square.x + i, square.y + j))
//                        result.Add(GetSquare(square.x + i, square.y + j));
            return result;
        }

        void FindCurrentSquare(Vector3 position)
        {
            if (!m_Inited)
            {
                Log.Info("MapManager Not inited for square  find");
                return;
            }

            if (IsPointInSquare(m_CurrentSquare, position))
                return;
            ChangeCurrentSquare(FindSquareIndexForWorldPosition(position));
        }

        void ChangeCurrentSquare(Vector2Int newSquare)
        {
            ChangeCurrentSquare(newSquare.x, newSquare.y);
        }

        void ChangeCurrentSquare(int newX, int newY)
        {
            m_CurrentSquare.x = newX;
            m_CurrentSquare.y = newY;
        }

        bool IsPointInSquare(Vector2Int index, Vector3 position)
        {
            return IsPointInSquare(index.x, index.y, position);
        }

        bool IsPointInSquare(int x, int y, Vector3 position)
        {
            if (!CheckSquareIndexes(x, y))
                return false;
            return Mathf.Abs(m_UnitsSquares[x, y].Center.x - position.x) - SQUARE_SIZE / 2 < 0
                   && Mathf.Abs(m_UnitsSquares[x, y].Center.y - position.z) - SQUARE_SIZE / 2 < 0;
        }

        public Square GetSquare(Vector2Int currentSquare)
        {
            return GetSquare(currentSquare.x, currentSquare.y);
        }

        public Square GetSquare(int x, int y)
        {
            return CheckSquareIndexes(x, y) ? m_UnitsSquares[x, y] : null;
        }

        bool CheckSquareIndexes(Vector2Int indexes)
        {
            return CheckSquareIndexes(indexes.x, indexes.y);
        }

        bool CheckSquareIndexes(int x, int y)
        {
            return x >= 0 && y >= 0 && x < m_SquaresRows && y < m_SquaresColumns;
        }
        
        public void Visualize()
        {
            for (int i = 0; i < m_SquaresRows; i++)
                for (int j = 0; j < m_SquaresColumns; j++)
                    m_UnitsSquares[i, j].Visualize();
        }
    }

    public class Square
    {
        public Vector2Int PositionInGrid { get; }
        
        public ReactiveProperty<bool> Active = new ReactiveProperty<bool>();
        public Vector2 Center { get; }
        
        public readonly List<IUnit> Units = new List<IUnit>();

        float m_Size;

        Vector3 bottomLeft;
        Vector3 bottomRight;
        Vector3 upLeft;
        Vector3 upRight;
        
        public Square(Vector2 center, float size, int x, int y)
        {
            PositionInGrid = new Vector2Int(x, y);
            Center = center;
            m_Size = size;
            if (Debug.isDebugBuild)
                CalculateCorners();
        }

        public void Activate()
        {
            Active.Value = true;
        }

        public void Deactivate()
        {
            Active.Value = false;
        }

        public void AddUnit(IUnit unit)
        {
            if (!Units.Contains(unit))
                Units.Add(unit);
        }
        
        public void RemoveUnit(IUnit unit)
        {
            Units.Remove(unit);
        }
        
        void CalculateCorners()
        {
            float halfSize = m_Size / 2 - 0.5f;
            Vector3 CenterV3 = new Vector3(Center.x, 5, Center.y);
            bottomLeft = CenterV3 + new Vector3(-halfSize, 0, -halfSize);
            bottomRight = CenterV3 + new Vector3(-halfSize, 0, halfSize);
            upLeft = CenterV3 - new Vector3(-halfSize, 0, halfSize);
            upRight = CenterV3 + new Vector3(halfSize, 0, halfSize);
        }

        public void Visualize()
        {
            if (!Debug.isDebugBuild)
                return;
            if (!Active.Value)
                return;
            Color color = Active.Value ? Color.red : Color.blue; 
            Debug.DrawLine(bottomLeft, bottomRight, color);
            Debug.DrawLine(bottomLeft, upLeft, color);
            Debug.DrawLine(upLeft, upRight, color);
            Debug.DrawLine(upRight, bottomRight, color);
        }
    }

    public class PlayerOutOfSquaresSpaceException : Exception
    {
    
    }
}