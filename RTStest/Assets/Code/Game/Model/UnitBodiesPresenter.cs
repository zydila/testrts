﻿using System;
using System.Collections.Generic;
using Code.Core;
using Code.Game.Model.Units;
using Code.Game.Model.Units.Interfaces;
using UniRx;

namespace Code.Game.Model
{
    public class UnitBodiesPresenter
    {
        readonly List<BodyHandler> m_UnitsBodiesHandler = new List<BodyHandler>();

        public void AddUnit(IUnit unit)
        {
            m_UnitsBodiesHandler.Add(new BodyHandler(unit));
        }

        class BodyHandler : IDisposable
        {
            readonly IUnit Unit;
            
            Square Square;
            UnitBody Body;
            
            IDisposable m_SquareHandler;
            IDisposable m_ActiveHandler;
            
            readonly UnitBodyBuilder<UnitBody> m_BodyBuilder = new UnitBodyBuilder<UnitBody>();

            public BodyHandler(IUnit unit)
            {
                Unit = unit;
                m_SquareHandler?.Dispose();
                m_SquareHandler = Unit.CurrentSquare.Subscribe(OnSquareChanged);
                Unit.Destroy.DoOnCompleted(Dispose);
            }

            void OnSquareChanged(Square newSquare)
            {
                Square = newSquare;
                m_ActiveHandler?.Dispose();
                m_ActiveHandler = newSquare.Active.Subscribe(OnActiveChanged);
            }

            void OnActiveChanged(bool isActive)
            {
//                Log.Info($"handle squareChange, pos: {Unit.Position.Value}, Square: {Square.Center} {Square.Active.Value}");
                if (isActive)
                {
                    if (Body == null)
                        BuildBody();
                }
                else
                    DestroyBody();
            }
            
            void BuildBody()
            {
                m_BodyBuilder.Clean();
                Body = m_BodyBuilder.For(Unit).Build();
            }

            void DestroyBody()
            {
                if (Body != null) 
                    Body.Destroy();
                Body = null;
            }

            public void Dispose()
            {
                DestroyBody();
                m_SquareHandler?.Dispose();
                m_ActiveHandler?.Dispose();
            }
        }
    }
}