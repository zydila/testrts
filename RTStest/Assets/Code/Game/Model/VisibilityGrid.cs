﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Code.Core;
using Code.Core.Managers;
using Code.Game.Model.Units.Interfaces;
using Newtonsoft.Json;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Code.Game.Model
{
    public class VisibilityGrid
    {
        [Inject] EventDispatcher m_EventDispatcher;
        
        public const float VISIBILITY_CELL_SIZE = 5f;
        public const float MAXIMAL_VISIBILITY_DETECTION_RADIUS = 180f; //TODO MAXIMAL VISIBILITY RADIUS should be global or delete
        public const char VISIBILITY_GRID_SAVE_DELIMITER = '|';
        public const string VISIBILITY_GRID_SAVE_FILE = "Assets/Resources/PredefinedConfig/VisibilityGridSave_{0}.txt";
        const string VISIBILITY_GRID_SAVE_FILE_IN_RESOURCES = "PredefinedConfig/VisibilityGridSave_";

        VisibilityCell[,] m_VisibilityCells;

        readonly int m_SquaresRows;
        readonly int m_SquaresColumns;
        bool m_Inited;

        float m_StartX;
        float m_StartY;
        
        bool centerBlocked;
        bool cornerLeftBlocked;
        bool cornerRightBlocked;
        int indexX;
        int indexY;
        int indexXSign;
        int indexYSign;
        Vector2 fromCenterV2 = new Vector2();
        Vector2 toCenterV2 = new Vector2();
        
        Vector2 fromRelative = new Vector2();
        Vector2 directionLeft = new Vector2();
        Vector2 directionRight = new Vector2();
        Vector2 directionCenter = new Vector2();
        
        Vector2 blockerCenterV2 = new Vector2();
        Vector2 blockerRelative = new Vector2();
        Vector2 directionBlockerLeftUp = new Vector2();
        Vector2 directionBlockerRightBottom = new Vector2();
        
        Vector3 normalizedDirectionBlocker = new Vector3();
        Vector3 normalizedDirection = new Vector3();

        readonly Vector2 leftUp = new Vector2(-0.5f, 0.5f);
        readonly Vector2 rightBottom = new Vector2(0.5f, -0.5f);

        string[] LoadFromFile()
        {
            var textAssets = Resources.Load<TextAsset>(VISIBILITY_GRID_SAVE_FILE_IN_RESOURCES + SceneManager.GetActiveScene().name);
            return textAssets.text.Split(VISIBILITY_GRID_SAVE_DELIMITER);
        }
        
        public VisibilityGrid(Vector3 mapBound)
        {
            this.Inject();
            m_StartX = -mapBound.x / 2 + VISIBILITY_CELL_SIZE / 2;
            m_StartY = -mapBound.z / 2 + VISIBILITY_CELL_SIZE / 2;
            m_SquaresRows = Mathf.CeilToInt(mapBound.x / VISIBILITY_CELL_SIZE);
            m_SquaresColumns = Mathf.CeilToInt(mapBound.z / VISIBILITY_CELL_SIZE);
            m_VisibilityCells = new VisibilityCell[m_SquaresRows, m_SquaresColumns];
            string[] savedHeight = LoadFromFile();
            for (int i = 0; i < m_SquaresRows; i++)
                for (int j = 0; j < m_SquaresColumns; j++)
                {
                    Vector3 center = new Vector3(m_StartX + VISIBILITY_CELL_SIZE * i, float.Parse(savedHeight[j + i * m_SquaresColumns]), m_StartY + VISIBILITY_CELL_SIZE * j);
                    m_VisibilityCells[i, j] = new VisibilityCell(center, i, j);
                }
            m_Inited = true;
            //TODO: manage GC
            Resources.UnloadUnusedAssets();
            GC.Collect();
//            if (Debug.isDebugBuild)
//                m_EventDispatcher.OnUpdate += Visualize;
        }

        public VisibilityCell GetVisibilitySquare(Vector3 position)
        {
            int x = Mathf.RoundToInt((position.x - m_StartX) / VISIBILITY_CELL_SIZE);
            int y = Mathf.RoundToInt((position.z - m_StartY) / VISIBILITY_CELL_SIZE);
            x = Mathf.Clamp(x, 0, m_SquaresRows);
            y = Mathf.Clamp(y, 0, m_SquaresColumns);
            return m_VisibilityCells[x, y];
        }

        public void CheckVisibilityForAllSquares(Vector3 fromPosition, float visibilityRadius)
        {
            int fromX = Mathf.RoundToInt((fromPosition.x - m_StartX) / VISIBILITY_CELL_SIZE);
            int fromY = Mathf.RoundToInt((fromPosition.z - m_StartY) / VISIBILITY_CELL_SIZE);
            int visibilityRadiusInCells = Mathf.CeilToInt(visibilityRadius / VISIBILITY_CELL_SIZE);
            var square = m_VisibilityCells[fromX, fromY];
            square.Visualize(Color.white);
            for(int n = 1; n < visibilityRadiusInCells; n++)
                for(int i = -n; i <= n; i++)
                    for (int j = -n; j <= n; j++)
                    {
                        if (Mathf.Abs(i) < n && Mathf.Abs(j) < n)
                            continue;
                        int checkingX = square.X + i;
                        int checkingY = square.Y + j;
                        if (checkingX < 0 || checkingX >= m_SquaresRows || checkingY < 0 || checkingY >= m_SquaresColumns)
                            continue;
                        if ((fromPosition - m_VisibilityCells[checkingX, checkingY].CenterV3).magnitude > visibilityRadius)
                        {
                            m_VisibilityCells[checkingX, checkingY].Visualize(Color.black);
                            continue;
                        }

                        if (CheckVisibility(square, m_VisibilityCells[checkingX, checkingY]))
                            m_VisibilityCells[checkingX, checkingY].Visualize(Color.blue);
                        else
                            m_VisibilityCells[checkingX, checkingY].Visualize(Color.yellow);
                    }
        } 
        
        public void Visualize()
        {
            for (int i = 0; i < m_SquaresRows; i++)
                for (int j = 0; j < m_SquaresColumns; j++)
                    m_VisibilityCells[i, j].Visualize(Color.blue);
        }
        public bool CheckVisibility(Vector3 fromPosition, Vector3 toPosition)
        {
            int fromX = Mathf.RoundToInt((fromPosition.x - m_StartX) / VISIBILITY_CELL_SIZE);
            int fromY = Mathf.RoundToInt((fromPosition.z - m_StartY) / VISIBILITY_CELL_SIZE);
            int toX = Mathf.RoundToInt((toPosition.x - m_StartX) / VISIBILITY_CELL_SIZE);
            int toY = Mathf.RoundToInt((toPosition.z - m_StartY) / VISIBILITY_CELL_SIZE);
            return CheckVisibility(m_VisibilityCells[fromX, fromY], m_VisibilityCells[toX, toY]);
        }
        
        bool CheckVisibility(VisibilityCell from, VisibilityCell to)
        {
            if (from.X == to.X && from.Y == to.Y)
                return false;
            centerBlocked = false;
            cornerLeftBlocked = false;
            cornerRightBlocked = false;
            
            indexX = to.X - from.X;
            indexY = to.Y - from.Y;
            indexXSign = indexX < 0 ? -1 : 1;
            indexYSign = indexY < 0 ? -1 : 1;
            
            indexX = Mathf.Abs(indexX);
            indexY = Mathf.Abs(indexY);

            fromCenterV2.Set(from.X, from.Y);
            toCenterV2.Set(to.X, to.Y);
            
            fromRelative = (toCenterV2 - fromCenterV2).Abs();
            directionLeft = fromRelative + leftUp;
            directionRight = fromRelative + rightBottom;
            directionLeft = directionLeft / directionLeft.magnitude;
            directionRight = directionRight / directionRight.magnitude;
            if (directionRight.y < 0)
                directionRight.x = 1;
            directionCenter = fromRelative.normalized;
            for(int i = 0; i <= indexX; i++)
            {
                for (int j = 0; j <= indexY; j++)
                {
                    if (i == 0 && j == 0)
                        continue;
                    int checkingBlockerIndexX = from.X + indexXSign * i;
                    int checkingBlockerIndexY = from.Y + indexYSign * j;

                    if (from.CenterV3.y > m_VisibilityCells[checkingBlockerIndexX, checkingBlockerIndexY].CenterV3.y &&
                        to.CenterV3.y > m_VisibilityCells[checkingBlockerIndexX, checkingBlockerIndexY].CenterV3.y)
                        continue;
                    if (Mathf.Approximately(from.CenterV3.y, m_VisibilityCells[checkingBlockerIndexX, checkingBlockerIndexY].CenterV3.y) &&
                        Mathf.Approximately(to.CenterV3.y, m_VisibilityCells[checkingBlockerIndexX, checkingBlockerIndexY].CenterV3.y))
                        continue;
                    blockerCenterV2.Set(m_VisibilityCells[checkingBlockerIndexX, checkingBlockerIndexY].X, m_VisibilityCells[checkingBlockerIndexX, checkingBlockerIndexY].Y);
                    blockerRelative = (blockerCenterV2 - fromCenterV2).Abs();
                    directionBlockerLeftUp = blockerRelative + leftUp;
                    directionBlockerRightBottom = blockerRelative + rightBottom;
                    directionBlockerLeftUp = directionBlockerLeftUp / directionBlockerLeftUp.magnitude;
                    directionBlockerRightBottom = directionBlockerRightBottom / directionBlockerRightBottom.magnitude;
                    
                    if (directionBlockerRightBottom.y < 0)
                        directionBlockerRightBottom.x = 1.1f;
                    normalizedDirectionBlocker = m_VisibilityCells[checkingBlockerIndexX, checkingBlockerIndexY].CenterV3 - Vector3.up - from.CenterV3;
                    normalizedDirection = to.CenterV3 - from.CenterV3;
                    normalizedDirectionBlocker = normalizedDirectionBlocker / normalizedDirectionBlocker.magnitude;
                    normalizedDirection = normalizedDirection / normalizedDirection.magnitude;
                    
                    if (normalizedDirectionBlocker.y < normalizedDirection.y || Mathf.Abs(normalizedDirectionBlocker.y - normalizedDirection.y) < 0.01f)
                        continue;

                    if (!cornerLeftBlocked && (
                                                  (Mathf.Approximately(directionLeft.x, directionBlockerLeftUp.x) &&
                                                   Mathf.Approximately(directionLeft.y, directionBlockerLeftUp.y)) ||
                                                  (Mathf.Approximately(directionLeft.x, directionBlockerRightBottom.x) &&
                                                   Mathf.Approximately(directionLeft.y, directionBlockerRightBottom.y))))
                        cornerLeftBlocked = true;

                    if (!cornerRightBlocked && (
                                                   (Mathf.Approximately(directionRight.x, directionBlockerLeftUp.x) &&
                                                    Mathf.Approximately(directionRight.y, directionBlockerLeftUp.y)) ||
                                                   (Mathf.Approximately(directionRight.x, directionBlockerLeftUp.x) &&
                                                    Mathf.Approximately(directionRight.y, directionBlockerLeftUp.y))))
                        cornerRightBlocked = true;

                    if (!centerBlocked && (
                                              (Mathf.Approximately(directionCenter.x, directionBlockerLeftUp.x) &&
                                               Mathf.Approximately(directionCenter.y, directionBlockerLeftUp.y)) ||
                                              (Mathf.Approximately(directionCenter.x, directionBlockerLeftUp.x) &&
                                               Mathf.Approximately(directionCenter.y, directionBlockerLeftUp.y))))
                        centerBlocked = true;

                    if (!cornerLeftBlocked &&
                        directionLeft.x > directionBlockerLeftUp.x &&
                        directionLeft.x < directionBlockerRightBottom.x)
                        cornerLeftBlocked = true;

                    if (!cornerRightBlocked &&
                        directionRight.x < directionBlockerRightBottom.x &&
                        directionRight.x > directionBlockerLeftUp.x)
                        cornerRightBlocked = true;

                    if (!centerBlocked &&
                        directionCenter.x < directionBlockerRightBottom.x &&
                        directionCenter.x > directionBlockerLeftUp.x)
                        centerBlocked = true;

                    if (cornerLeftBlocked && centerBlocked && cornerRightBlocked)
                        return false;
                }
            }
            return true;
        }

        public class VisibilityCell
        {
            public readonly Vector3 CenterV3;
            public readonly int X;
            public readonly int Y;

            readonly Vector3 bottomLeft;
            readonly Vector3 bottomRight;
            readonly Vector3 upLeft;
            readonly Vector3 upRight;
            
            public VisibilityCell(Vector3 center, int x, int y)
            {
                X = x;
                Y = y;
                CenterV3 = center;
                
                float halfSize = VISIBILITY_CELL_SIZE / 2 - 0.1f;
                bottomLeft = CenterV3 + new Vector3(-halfSize, 0, -halfSize);
                bottomRight = CenterV3 + new Vector3(-halfSize, 0, halfSize);
                upLeft = CenterV3 - new Vector3(-halfSize, 0, halfSize);
                upRight = CenterV3 + new Vector3(halfSize, 0, halfSize);
            }

            public void Visualize(Color color)
            {
                if (!Debug.isDebugBuild)
                    return;
                Debug.DrawLine(bottomLeft, bottomRight, color, 100f);
                Debug.DrawLine(bottomLeft, upLeft, color, 100f);
                Debug.DrawLine(upLeft, upRight, color, 100f);
                Debug.DrawLine(upRight, bottomRight, color, 100f);
            }
        }
    }
}