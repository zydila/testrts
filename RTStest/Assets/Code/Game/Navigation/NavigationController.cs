﻿using System;
using System.Collections.Generic;
using Code.Core;
using Code.Core.Managers;
using Code.Game.Model.Units;
using UnityEngine;
using UnityEngine.AI;

namespace Code.Game.Model.Navigation
{
    public class NavigationController : MonoBehaviour
    {
        [Inject] NavigationManager m_NavigationManager;

        [SerializeField] Transform fromTransform;
        [SerializeField] Transform toTransform;

        readonly Dictionary<UnitElement, int> m_NavigationAreasForElement = new Dictionary<UnitElement, int>
        {
            { UnitElement.All, NavMesh.AllAreas },
            { UnitElement.Ground, (1 << GlobalConstants.NAVMESH_GROUND_AREA) 
                                  | (1 << GlobalConstants.NAVMESH_GROUND_ROAD_AREA) },
            { UnitElement.Water, (1 << GlobalConstants.NAVMESH_WATER_AREA) },
            { UnitElement.Air, (1 << GlobalConstants.NAVMESH_WATER_AREA) },
            { UnitElement.Amphibious, (1 << GlobalConstants.NAVMESH_GROUND_AREA) 
                                      | (1 << GlobalConstants.NAVMESH_GROUND_ROAD_AREA) 
                                      | (1 << GlobalConstants.NAVMESH_WATER_AREA) },
        };

        NavMeshPath m_Path;
        NavMeshHit m_NavMeshHit;
        
        void Awake()
        {
            this.Inject();
            m_NavigationManager.SetNavigationController(this);
            m_Path = new NavMeshPath();
        }

        public bool CalculatePath(Vector3 from, Vector3 to, UnitElement element, out Vector3[] corners)
        {
            corners = null;
            if (m_Path == null)
                return false;
            NavMesh.CalculatePath(from, to, m_NavigationAreasForElement[element], m_Path);
            if (m_Path.status == NavMeshPathStatus.PathComplete)
                corners = m_Path.corners;
            return m_Path.status == NavMeshPathStatus.PathComplete;
        }
        
        public bool GetClosestNavPoint(Vector3 point, UnitElement element, out Vector3 closestPointOnNavMesh)
        {
            if (NavMesh.SamplePosition(point, out m_NavMeshHit, 300f, m_NavigationAreasForElement[element]))
            {
                closestPointOnNavMesh = m_NavMeshHit.position;
                return true;
            }

            closestPointOnNavMesh = point;
            return false;
        }

        void FixedUpdate()
        {
            CalculatePath(fromTransform.position, toTransform.position, UnitElement.All, out Vector3[] corners);
        }

        void OnDrawGizmos()
        {
//            Gizmos.color = Color.red;
//            Gizmos.DrawSphere(fromTransform.position, 2);
//            Gizmos.DrawSphere(toTransform.position, 2);
//            if (m_Path != null && m_Path.status == NavMeshPathStatus.PathComplete)
//                for (int i = 0; i < m_Path.corners.Length - 1; i++)
//                    Debug.DrawLine(m_Path.corners[i], m_Path.corners[i + 1], Color.red);
        }
    }
}