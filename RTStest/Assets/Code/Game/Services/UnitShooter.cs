﻿using Code.Core.Managers;
using Code.Game.Model.Services;
using Code.Game.Model.Units;
using Code.Game.Model.Units.Interfaces;
using UniRx;
using UnityEngine;

namespace Code.Game.Services {
    public class UnitShooter : IService
    {
        [Inject] ServicesManager m_ServicesManager;

        public IReadOnlyReactiveProperty<bool> InPlayerView => m_InPlayerView;
        ReactiveProperty<bool> m_InPlayerView = new ReactiveProperty<bool>();

        readonly ICombatUnit m_CombatUnit;
        IDamageable m_Target;
        float m_StartedPrepare = -1f;

        bool m_Aimed;

        public UnitShooter(ICombatUnit combatUnit)
        {
            this.Inject();
            m_CombatUnit = combatUnit;
            m_ServicesManager.ServicesScheduler.Add(this);
        }

        public void SetTarget(IDamageable target)
        {
            m_Target = target;
        }
        
        public void Start()
        {
            m_StartedPrepare = -1f;
        }
        
        public bool Execute()
        {
            if (!m_CombatUnit.PreparedForShooting.Value)
                PrepareToShoot();
            else if (m_Aimed)
                Shoot();
            else
                Aim();
            return true;
        }

        WeaponShooter m_WeaponShooter;
        void Shoot()
        {
            if (m_WeaponShooter == null)
            {
                m_WeaponShooter = new WeaponShooter(m_CombatUnit.SelectedWeapon, m_Target);
                m_WeaponShooter.Start();
            }
            m_WeaponShooter.Execute();
            StopAndClear();
        }

        void PrepareToShoot()
        {
            if (m_StartedPrepare < 0)
                m_StartedPrepare = Time.timeSinceLevelLoad;
            if (Time.timeSinceLevelLoad - m_StartedPrepare > m_CombatUnit.PreparationTime)
                m_CombatUnit.SetPreparedForShooting(true);
        }

        void Aim()
        {
            if (m_CombatUnit is IMovable movable && m_Target is ITransformable targetTransformable)
            {
                movable.SetNewRotation(Quaternion.LookRotation(targetTransformable.Position.Value - m_CombatUnit.Position.Value));
                m_Aimed = true;
            }
            else
                m_Aimed = true;
        }

        public void StopAndClear()
        {
            m_ServicesManager.ServicesScheduler.Remove(this);
        }
        
        public void Dispose()
        {
            
        }
    }
}
