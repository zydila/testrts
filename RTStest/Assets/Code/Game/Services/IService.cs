﻿using System;
using Code.Game.Model.Units.Tasks;
using UniRx;

namespace Code.Game.Model.Services {
    public interface IService : IDisposable
    {
        IReadOnlyReactiveProperty<bool> InPlayerView { get; }
        
        void Start();
        void StopAndClear();

        bool Execute();
    }
}
