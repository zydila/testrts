﻿using Code.Core.Managers;
using Code.Game.Model.Services;
using Code.Game.Model.Units;
using Code.Game.Model.Units.Interfaces;
using Code.Game.Model.Units.Weapons;
using UniRx;
using UnityEngine;

namespace Code.Game.Services {
    public class WeaponShooter : IService
    {
        [Inject] ServicesManager m_ServicesManager;
        
        public IReadOnlyReactiveProperty<bool> InPlayerView { get; }
        WeaponBase m_Weapon;
        
        IDamageable m_Target;
        Vector3 m_TargetLocation;

        public WeaponShooter(WeaponBase weapon, IDamageable target)
        {
            this.Inject();
            m_Weapon = weapon;
            m_Target = target;
        }

        public WeaponShooter(WeaponBase weapon, Vector3 location)
        {
            this.Inject();
            m_Weapon = weapon;
            m_TargetLocation = location;
        }
    
        public void Start()
        {
            m_Weapon.Projectile.SetPosition(m_Weapon.m_Owner.Position.Value);
        }

        public void StopAndClear()
        {
        }

        public bool Execute()
        {
            m_Target.ApplyDamage(m_Weapon.Projectile.DamageType, m_Weapon.Projectile.Power);
            StopAndClear();
            return true;
        }

        public void Dispose()
        {
        
        }
    }
}
