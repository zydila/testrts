﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Code.Core;
using Code.Core.Managers;
using Code.Game.Model.Units;
using Code.Game.Model.Units.Interfaces;
using Code.Game.Model.Units.Tasks;
using UniRx;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Profiling;

namespace Code.Game.Model.Services
{
    public class UnitDetector : IService
    {
        [Inject] ServicesManager m_ServicesManager;
        [Inject] MapManager m_MapManager;
        [Inject] UnitsManager m_UnitsManager;

        public IReadOnlyReactiveProperty<bool> InPlayerView => m_InPlayerView;
        public ReactiveProperty<bool> m_InPlayerView = new ReactiveProperty<bool>();
        
        IUnit m_Detector;
        float m_LastDetectTick = -1;

        readonly float m_DetectInterval;
        
        IDisposable m_squareChangeHandler;
        IDisposable m_squareActiveChangedHandler;

        public UnitDetector(IUnit detector, float detectInterval = 1f)
        {
            this.Inject();
            m_Detector = detector;
            m_DetectInterval = detectInterval;
        }

        public void Start()
        {
            if (m_Detector == null)
            {
                Log.Warning("Detector not inited");
                return;
            }
            m_squareChangeHandler = m_Detector.CurrentSquare.Subscribe(OnSquareChanged);
            m_ServicesManager.ServicesScheduler.Add(this);
        }

        void OnSquareChanged(Square square)
        {
            m_squareActiveChangedHandler?.Dispose();
            m_squareActiveChangedHandler = square.Active.Subscribe(active => m_InPlayerView.Value = active);
        }

        public bool Execute()
        {
            if (!m_Detector.CanDetect.Value)
                return false;
            if (Time.timeSinceLevelLoad - m_LastDetectTick < m_DetectInterval)
                return false;
            m_LastDetectTick = Time.timeSinceLevelLoad;
            int gridSearchRadius = Mathf.CeilToInt(m_Detector.DetectRange[DetectChannel.Visibility] / MapGrid.SQUARE_SIZE);
            Vector2Int[] squares = m_MapManager.MapGrid.GetSquaresAround(m_Detector.CurrentSquare.Value, gridSearchRadius);
            for (var i = 0; i < squares.Length; i++)
            {
                foreach (IUnit detectable in m_MapManager.MapGrid.GetSquare(squares[i]).Units)
                {
                    if (detectable.Faction == m_Detector.Faction) 
                        continue;
                    
                    if (detectable.IsDetected.Value && detectable.LastDetectedFrame == Time.frameCount) //TODO: system to remember detected for some time
                        continue;
                    
                    if (!detectable.DetectableCoef.ContainsKey(DetectChannel.Visibility))
                        continue;
                    
                    if ((detectable.Position.Value - m_Detector.Position.Value).magnitude > m_Detector.DetectRange[DetectChannel.Visibility]) 
                        continue;

                    //TODO: Check if hidden
                    
                    if (m_MapManager.VisibilityGrid.CheckVisibility(m_Detector.Position.Value, detectable.Position.Value))
                        detectable.Detect();
                    
//                    if (m_UnitsManager.UnitVisibilityChecker.CheckVisibility(m_Detector, detectable))
//                    {
//                        detectable.Detect();
//                        detectables.Add(detectable);
//                    }
                }
            }

            return true;
        }

        public void StopAndClear()
        {
            m_Detector = null;
        }

        public void Dispose()
        {
            m_ServicesManager.ServicesScheduler.Remove(this);
            m_squareActiveChangedHandler?.Dispose();
            m_squareChangeHandler?.Dispose();
            StopAndClear();
        }
    }
}