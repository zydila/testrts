﻿using System;
using Code.Core;
using Code.Core.Managers;
using Code.Game.Model.Units;
using Code.Game.Model.Units.Tasks;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

namespace Code.Game.Model.Services {
    public class UnitMover : IService
    {
        [Inject] NavigationManager m_NavigationManager;
        [Inject] EventDispatcher m_EventDispatcher;
        [Inject] ServicesManager m_ServicesManager;

        public IReadOnlyReactiveProperty<bool> InPlayerView => m_InPlayerView;
        ReactiveProperty<bool> m_InPlayerView = new ReactiveProperty<bool>();

        IMovable m_Movable;
        
        Vector3 m_NewPosition;
        Quaternion m_NewRotation;
        NavMeshHit m_NavMeshHit;
        RaycastHit m_HitInfo;
        
        float m_AllowanceRadius;
        Vector3[] m_PathCorners;

        int m_CurrentCorner = 1;
        bool m_Completed;
        bool m_Stopped;
        bool m_Started;
        bool m_NoPath;
        MoveTask m_MoveTask;
        float m_LastExecuteTime = -1;
        float m_DeltaTime = 0.1f;

        IDisposable m_squareChangeHandler;
        IDisposable m_squareActiveChangedHandler;
        
        public UnitMover(IMovable movable, MoveTask moveTask, float allowanceRadius = 5f)
        {
            this.Inject();
            m_Movable = movable;
            m_MoveTask = moveTask;
            m_AllowanceRadius = allowanceRadius;
        }

        public void Start()
        {
            if (m_Movable == null)
            {
                Log.Warning("Mover not inited");
                return;
            }
            m_MoveTask.Start();
            m_NoPath = false;
            m_Completed = false;
            m_Stopped = false;
            m_Started = true;
            m_LastExecuteTime = -1;
            m_squareChangeHandler = m_Movable.CurrentSquare.Subscribe(OnSquareChanged);
            if (m_NavigationManager.GetClosestNavPoint(m_Movable.Position.Value, m_Movable.Element, out Vector3 result))
                m_Movable.SetNewPosition(result);
            CalculatePath();
            m_ServicesManager.ServicesScheduler.Add(this);
        }

        void OnSquareChanged(Square square)
        {
            m_squareActiveChangedHandler?.Dispose();
            m_squareActiveChangedHandler = square.Active.Subscribe(active => m_InPlayerView.Value = active);
        }

        void CalculatePath()
        {
            if (m_NavigationManager.NavigationController.CalculatePath(m_Movable.Position.Value, m_MoveTask.Destination, m_Movable.Element, out m_PathCorners))
            {
                for (var i = 0; i < m_PathCorners.Length; i++)
                    if (m_NavigationManager.GetClosestNavPoint(m_PathCorners[i], m_Movable.Element, out Vector3 adjustedToNavMesh))
                        m_PathCorners[i] = adjustedToNavMesh;

                //m_EventDispatcher.OnUpdate += Execute;
            }
            else
            {
                //Log.Warning($"NO PATH from {m_Movable.Position.Value} to {m_MoveTask.Destination}");
                StopAndClear();
                m_NoPath = true;
                //m_MoveTask.Error(new NoPathException());
            }
        }

        public void StopAndClear()
        {
            //m_EventDispatcher.OnUpdate -= Execute;
            m_Movable = null;
            m_Stopped = true;
            m_MoveTask = null;
            m_Started = false;
            m_NoPath = false;
            m_LastExecuteTime = -1;
            m_AllowanceRadius = 1f;
            m_ServicesManager.ServicesScheduler.Remove(this);
        }

        public bool Execute()
        {
            if (!m_Started || !m_Movable.CanMove.Value || m_Completed || m_NoPath)
                return false;
            if (m_LastExecuteTime == -1)
                m_DeltaTime = Time.deltaTime;
            else
                m_DeltaTime = Time.timeSinceLevelLoad - m_LastExecuteTime;
            m_LastExecuteTime = Time.timeSinceLevelLoad;
            CalculateNewTransform();
            m_Movable.SetNewPosition(m_NewPosition);
            m_Movable.SetNewRotation(m_NewRotation);
            CheckCompletion();
            return true;
        }

        void CalculateNewTransform()
        {
            Vector3 targetDirection = m_PathCorners[m_CurrentCorner] - m_Movable.Position.Value;
            float stoppingDistance = CalculateDecelerationDistance(CalculateDesiredVelocity());
            float distanceLeft = targetDirection.magnitude;
            
            if (stoppingDistance >= distanceLeft)
                DecelerateTo(0);
            else if (m_Movable.Velocity < m_Movable.MaxVelocity)
                AccelerateTo(m_Movable.MaxVelocity);
            
            float velocity = Mathf.Min(m_Movable.Velocity * m_DeltaTime, distanceLeft);
            m_NewPosition = targetDirection.normalized * velocity + m_Movable.Position.Value;
            ProjectPositionToScene();
            
            m_NewRotation = Quaternion.LookRotation(m_NewPosition - m_Movable.Position.Value);
            if (!m_Movable.Rotation.Value.Equals(m_NewRotation))
                m_NewRotation = Quaternion.Slerp(m_Movable.Rotation.Value, m_NewRotation, (m_Movable.Rotation.Value.eulerAngles - m_NewRotation.eulerAngles).magnitude / m_Movable.RotationSpeed * m_DeltaTime);
        }

        void ProjectPositionToScene()
        {
            if (m_Movable.CurrentSquare.Value.Active.Value)
            {
                if (Physics.Raycast(m_NewPosition + Vector3.up * 10f, Vector3.down, out m_HitInfo, 50f, GlobalConstants.VISIBILITY_TERRAIN_AND_WATER_MASK))
                    m_NewPosition.y = m_HitInfo.point.y;
            }
            else
            {
                if (NavMesh.SamplePosition(m_NewPosition, out m_NavMeshHit, 50f, NavMesh.AllAreas))
                    m_NewPosition.y = m_NavMeshHit.position.y;
            }
        }

        void CheckCompletion()
        {
            if ((m_PathCorners[m_CurrentCorner] - m_Movable.Position.Value).magnitude < m_AllowanceRadius && m_CurrentCorner != m_PathCorners.Length - 1)
                m_CurrentCorner++;
            m_Completed = (m_CurrentCorner == m_PathCorners.Length - 1 && m_AllowanceRadius > (m_PathCorners[m_CurrentCorner] - m_Movable.Position.Value).magnitude 
                                                                   && Math.Abs(m_Movable.Velocity) < m_Movable.Acceleration * 0.5f);
            if (m_Completed)
            {
                m_Movable.Velocity = 0f;
                m_MoveTask.Complete();
            }
        }

        float CalculateDecelerationDistance(float desiredVelocity)
        {
            //linear acceleration stopping distance
            return (Mathf.Pow(m_Movable.Velocity, 2f) - Mathf.Pow(desiredVelocity,2)) / 2 / m_Movable.Acceleration; 
        }

        float CalculateDesiredVelocity()
        {
            float desiredVelocityCoef = 0;
            if (m_CurrentCorner > 0 && m_PathCorners.Length > 2 && m_CurrentCorner < m_PathCorners.Length - 1)
                desiredVelocityCoef = 1 - Vector3.Angle(m_PathCorners[m_CurrentCorner] - m_PathCorners[m_CurrentCorner - 1], m_PathCorners[m_CurrentCorner + 1] - m_PathCorners[m_CurrentCorner]) / 180;

            float desiredVelocity = desiredVelocityCoef * m_Movable.Velocity;
            if (m_CurrentCorner == m_PathCorners.Length - 1)
                desiredVelocity = 0;
            return desiredVelocity;
        }

        void AccelerateTo(float velocity)
        {
            m_Movable.Velocity = Mathf.Min(m_Movable.Velocity  + m_DeltaTime * m_Movable.Acceleration, velocity);
        }
        
        void DecelerateTo(float toSpeed)
        {
            m_Movable.Velocity = Mathf.Max(m_Movable.Velocity - m_DeltaTime * m_Movable.Acceleration, toSpeed);
        }

        public void Dispose()
        {
            m_squareActiveChangedHandler?.Dispose();
            m_squareChangeHandler?.Dispose();
            StopAndClear();
        }
    }
}

public class NoPathException : Exception
{
    
}
