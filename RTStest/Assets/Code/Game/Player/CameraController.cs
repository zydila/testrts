﻿using System;
using System.Collections.Generic;
using Code.Core;
using Code.Core.Common;
using Code.Core.Managers;
using UnityEngine;

namespace Code.Game.Model.Player {
    public class CameraController : MonoBehaviour, IObservable<Vector3>
    {
        [Inject] PlayerManager m_PlayerManager;
        [Inject] MapManager m_MapManager;
        
        [SerializeField] Camera m_Camera;
        [SerializeField] float distanceFromGround = DEFAULT_HEIGHT;
        
        public Camera MainCamera { get { return m_Camera; } }
        public Vector3 WorldCameraPosition { get { return m_CameraLocalPosition; } }
        public float CameraRotation { get { return m_RotY; } }
        public float RelativeHeight { get { return distanceFromGround / DEFAULT_HEIGHT; } }

        const float DEFAULT_HEIGHT = 30f;
        const float MAX_HEIGHT = 90f;
        const float MIN_HEIGHT = 1f;
        const float MAX_SEARCH_GROUND_DISTANCE = 150f;
        const float DEFAULT_CAMERA_MOVE_SPEED = 1f;
        const float DEFAULT_CAMERA_ROTATION_SPEED = 15f;
        const float MAX_X_ROTATION = 55f;
        const float DEFAULT_X_ROTATION = 40f;

        float AdjustedMoveSpeed
        {
            get { return (distanceFromGround / DEFAULT_HEIGHT) * DEFAULT_CAMERA_MOVE_SPEED; }
        }

        readonly List<IObserver<Vector3>> m_Observers = new List<IObserver<Vector3>>();
        
        Transform m_PlayerCameraTransform;
        float m_RotX = -DEFAULT_X_ROTATION;
        float m_RotY;
        Quaternion m_CameraRotation;
        Vector3 m_CameraLocalPosition;
        RaycastHit m_HitInfo;
        float m_TargetHeight;

        void Awake()
        {
            this.Inject();
            if (m_Camera == null)
                m_Camera = GetComponent<Camera>();
            m_PlayerCameraTransform = m_Camera.transform;
            m_PlayerManager.SetCameraController(this);
        }

        void Update()
        {
            //TODO: Deсide which is better
//            if (Physics.Raycast(transform.position + (Vector3.up * distanceFromGround), Vector3.down, out m_HitInfo, MAX_SEARCH_GROUND_DISTANCE + distanceFromGround, GlobalConstants.VISIBILITY_TERRAIN_AND_WATER_MASK))
//                m_CameraLocalPosition.y = m_HitInfo.point.y;
//            else
//                m_CameraLocalPosition.y = distanceFromGround;
            m_TargetHeight = m_MapManager.VisibilityGrid.GetVisibilitySquare(m_CameraLocalPosition).CenterV3.y + distanceFromGround;
            float deltaHeight = m_CameraLocalPosition.y - m_TargetHeight;
            m_CameraLocalPosition.y = m_CameraLocalPosition.y - Mathf.Sign(deltaHeight) * Mathf.Min(Mathf.Abs(deltaHeight), Time.deltaTime * 30f);
            transform.localPosition = m_CameraLocalPosition;
        }

        public void MoveCamera(Vector3 delta)
        {
            m_CameraLocalPosition += Quaternion.AngleAxis(m_RotY, Vector3.up)  * (delta * AdjustedMoveSpeed);
            NotifyPositionChanged();
        }

        public void SetCameraWorldPosition(Vector3 newPosition)
        {
            m_CameraLocalPosition = newPosition;
            NotifyPositionChanged();
        }

        public void ChangeDistanceFromGround(float delta)
        {
            distanceFromGround = Mathf.Clamp(distanceFromGround + delta, MIN_HEIGHT, MAX_HEIGHT);
        }
        
        public void RotateCamera(float deltaX, float deltaY)
        {
            m_RotX += -deltaX * Time.deltaTime * DEFAULT_CAMERA_ROTATION_SPEED;
            m_RotX = Mathf.Clamp(m_RotX, -MAX_X_ROTATION, 0);
            m_RotY += deltaY * Time.deltaTime * DEFAULT_CAMERA_ROTATION_SPEED;
            m_CameraRotation = Quaternion.Euler(m_RotX, m_RotY,0);
            transform.rotation = Quaternion.Euler(m_RotX, m_RotY,0);
        }

        public IDisposable Subscribe(IObserver<Vector3> observer)
        {
            if (!m_Observers.Contains(observer))
                m_Observers.Add(observer);
            observer.OnNext(m_CameraLocalPosition);
            return new EmptyDisposable();
        }

        void NotifyPositionChanged()
        {
            m_MapManager.MapGrid.OnPlayerChangedPosition(WorldCameraPosition);
            foreach (IObserver<Vector3> observer in m_Observers)
                observer.OnNext(m_CameraLocalPosition);
        }
    }
}
