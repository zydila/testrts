﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Code.Core;
using Code.Core.Managers;
using Code.Game.Model.Services;
using Code.Game.Model.Units;
using Code.Game.Model.Units.Interfaces;
using Code.Game.Model.Units.Tasks;
using Code.Game.Model.Units.Weapons;
using Code.Game.Services;
using UnityEngine;
using UniRx;
using Random = System.Random;

public class TestScript : MonoBehaviour
{
	void OnDrawGizmos()
	{
		foreach (IUnit unit in units)
		{
			//Gizmos.color = unit.CurrentSquare?.Value != null && unit.CurrentSquare.Value.Active.Value ? Color.red : Color.blue;
			//Gizmos.color = unit.Faction == Faction.Red ? Color.red : Color.blue;
			Gizmos.color = unit.IsDetected.Value ? Color.green : Color.red;
			Gizmos.DrawSphere(unit.Position.Value, 2f);
//			if (unit.IsDetected.Value)
//				Gizmos.DrawSphere(unit.Position.Value + new Vector3(0, 5f, 0), 1f);
		}
		if (unit2!=null)
		{
			Gizmos.color = Color.white;
			Gizmos.DrawSphere(unit2.Position.Value, 2f);
		}
	}

	[Inject] UnitsManager m_UnitsManager;
	[Inject] MapManager m_MapManager;
	[Inject] PlayerManager m_PlayerManager;
	[Inject] NavigationManager m_NavigationManager;

	List<IUnit> units = new List<IUnit>(); 
	IUnit unit2; 
	
	void Start()
	{
		this.Inject();
		Instantiate(navMeshObstacle);
		StartCoroutine(WaitAndStart(2, Test));
		//m_PlayerManager.CameraController.SetCameraWorldPosition(new Vector3(-1400, 0, -100));
	}

	[SerializeField] TroopUnitObserver troopTest;
	[SerializeField] Transform navMeshObstacle;
	
	[SerializeField] Transform debugUnitRoot;
	[SerializeField] Transform debugUnitPref;
	
	
	void Test()
	{
		//BuildUnits();
		//TestService();
		//TestServiceOne();
		//TestServiceMANY();
		//TestVisibility();
		TestShooting();
	}

	void TestService()
	{
		MoveTask moveTask = new MoveTask(TaskPriority.High, MovingMode.Walking, new Vector3(UnityEngine.Random.Range(-300, 300), 0, UnityEngine.Random.Range(-300, 300)));
		MoveTask moveTask1 = new MoveTask(TaskPriority.High, MovingMode.Walking, new Vector3(UnityEngine.Random.Range(-300, 300), 0, UnityEngine.Random.Range(-300, 300)));
		MoveTask moveTask2 = new MoveTask(TaskPriority.High, MovingMode.Walking, new Vector3(UnityEngine.Random.Range(-300, 300), 0, UnityEngine.Random.Range(-300, 300)));
		MoveTask moveTask3 = new MoveTask(TaskPriority.High, MovingMode.Walking, new Vector3(UnityEngine.Random.Range(-300, 300), 0, UnityEngine.Random.Range(-300, 300)));
		MoveTask moveTask4 = new MoveTask(TaskPriority.High, MovingMode.Walking, new Vector3(UnityEngine.Random.Range(-300, 300), 0, UnityEngine.Random.Range(-300, 300)));
		MoveTask moveTask5 = new MoveTask(TaskPriority.High, MovingMode.Walking, new Vector3(UnityEngine.Random.Range(-300, 300), 0, UnityEngine.Random.Range(-300, 300)));
		MoveTask moveTask6 = new MoveTask(TaskPriority.High, MovingMode.Walking, new Vector3(UnityEngine.Random.Range(-300, 300), 0, UnityEngine.Random.Range(-300, 300)));
		
		UnitObservable troop = new UnitObservable();
		UnitObservable troop1 = new UnitObservable();
		UnitObservable troop2 = new UnitObservable();
		UnitObservable troop3 = new UnitObservable();
		UnitObservable troop4 = new UnitObservable();
		UnitObservable troop5 = new UnitObservable();
		UnitObservable troop6 = new UnitObservable();
		
		units.Add(troop);
		units.Add(troop1);
		units.Add(troop2);
		units.Add(troop3);
		units.Add(troop4);
		units.Add(troop5);
		units.Add(troop6);
		
		m_UnitsManager.UnitBodiesPresenter.AddUnit(troop);
		m_UnitsManager.UnitBodiesPresenter.AddUnit(troop1);
		m_UnitsManager.UnitBodiesPresenter.AddUnit(troop2);
		m_UnitsManager.UnitBodiesPresenter.AddUnit(troop3);
		m_UnitsManager.UnitBodiesPresenter.AddUnit(troop4);
		m_UnitsManager.UnitBodiesPresenter.AddUnit(troop5);
		m_UnitsManager.UnitBodiesPresenter.AddUnit(troop6);
		
		troop.SetNewPosition(Vector3.one);// new Vector3(-50, 0, -50));
		troop1.SetNewPosition(Vector3.one);// new Vector3(50, 0, 50));
		troop2.SetNewPosition(Vector3.one);// new Vector3(100, 0, 100));
		troop3.SetNewPosition(Vector3.one);// new Vector3(-100, 0, 200));
		troop4.SetNewPosition(Vector3.one);// new Vector3(400, 0, 140));
		troop5.SetNewPosition(Vector3.one);// new Vector3(150, 0, -120));
		troop6.SetNewPosition(Vector3.one);// new Vector3(100, 0, -150)); 
		
		UnitMover mover = new UnitMover(troop, moveTask);
		UnitMover mover1 = new UnitMover(troop1, moveTask1);
		UnitMover mover2 = new UnitMover(troop2, moveTask2);
		UnitMover mover3 = new UnitMover(troop3, moveTask3);
		UnitMover mover4 = new UnitMover(troop4, moveTask4);
		UnitMover mover5 = new UnitMover(troop5, moveTask5);
		UnitMover mover6 = new UnitMover(troop6, moveTask6);
		
		mover.Start();
		mover1.Start();
		mover2.Start();
		mover3.Start();
		mover4.Start();
		mover5.Start();
		mover6.Start();
	}
	
	void TestServiceOne()
	{
		MoveTask moveTask = new MoveTask(TaskPriority.High, MovingMode.Walking, new Vector3(2000, 0, 450));
		UnitObservable troop = new UnitObservable();
		m_MapManager.MapGrid.AddUnitToGrid(troop);
		m_UnitsManager.AddUnit(troop);
		m_UnitsManager.UnitBodiesPresenter.AddUnit(troop);
		units.Add(troop);
		troop.SetNewPosition(new Vector3(-1400, 0, -100));
		UnitMover mover = new UnitMover(troop, moveTask);
		StartCoroutine(WaitAndStart(2f, mover.Start));
		//mover.Start();
	}
	
	void TestServiceMANY()
	{
		var range = m_MapManager.MapBound.Bounds.x / 2 - 40f;
		for (int i = 0; i <= 15000; i++)
		{
			UnitObservable troop = new UnitObservable();
			Transform debugUnit = Instantiate(debugUnitPref, debugUnitRoot);
			troop.Position.Subscribe(position => debugUnit.position = position);
			m_MapManager.MapGrid.AddUnitToGrid(troop);
			m_UnitsManager.AddUnit(troop);
			m_UnitsManager.UnitBodiesPresenter.AddUnit(troop);
			units.Add(troop);
			troop.SetNewPosition(new Vector3(UnityEngine.Random.Range(-range, range), 0, UnityEngine.Random.Range(-range, range)));
			m_NavigationManager.GetClosestNavPoint(
				new Vector3(UnityEngine.Random.Range(-range, range), 0, UnityEngine.Random.Range(-range, range)),
				UnitElement.Ground, out Vector3 Destination);
			MoveTask moveTask = new MoveTask(TaskPriority.High, MovingMode.Walking, Destination);//troop.Position.Value + new Vector3( UnityEngine.Random.Range(-130f, 130f), 0, UnityEngine.Random.Range(-130, 130)));
			UnitMover mover = new UnitMover(troop, moveTask);
			mover.Start();
			UnitDetector detector = new UnitDetector(troop);
				//detector.Start();
		}
		
		UnitObservable troop2 = new UnitObservable();
		troop2.Faction = Faction.Red;
		m_MapManager.MapGrid.AddUnitToGrid(troop2);
		m_UnitsManager.AddUnit(troop2);
		m_UnitsManager.UnitBodiesPresenter.AddUnit(troop2);
		m_NavigationManager.GetClosestNavPoint(
			new Vector3(UnityEngine.Random.Range(-range/2, range/2), 0, UnityEngine.Random.Range(-range/2, range/2)),
			UnitElement.Ground, out Vector3 pos);
		troop2.SetNewPosition(new Vector3(0,5f,0));
		unit2 = troop2;
		UnitDetector detector2 = new UnitDetector(troop2);
		detector2.Start();
		m_MapManager.VisibilityGrid.CheckVisibilityForAllSquares(troop2.Position.Value, troop2.DetectRange[DetectChannel.Visibility]);
	}
	
	void TestVisibility()
	{
		var range = m_MapManager.MapBound.Bounds.x / 2 - 40f;
		
		UnitObservable troop1 = new UnitObservable();
		troop1.Faction = Faction.Blue;
		m_MapManager.MapGrid.AddUnitToGrid(troop1);
		m_UnitsManager.AddUnit(troop1);
		m_UnitsManager.UnitBodiesPresenter.AddUnit(troop1);
		m_NavigationManager.GetClosestNavPoint(
			new Vector3(UnityEngine.Random.Range(-range/2, range/2), 0, UnityEngine.Random.Range(-range/2, range/2)),
			UnitElement.Ground, out Vector3 pos1);
		troop1.SetNewPosition(new Vector3(-70, 5, 50));
		units.Add(troop1);
		
		UnitObservable troop2 = new UnitObservable();
		troop2.Faction = Faction.Red;
		m_MapManager.MapGrid.AddUnitToGrid(troop2);
		m_UnitsManager.AddUnit(troop2);
		m_UnitsManager.UnitBodiesPresenter.AddUnit(troop2);
		m_NavigationManager.GetClosestNavPoint(
			new Vector3(UnityEngine.Random.Range(-range/2, range/2), 0, UnityEngine.Random.Range(-range/2, range/2)),
			UnitElement.Ground, out Vector3 pos2);
		troop2.SetNewPosition(new Vector3(50, 5, 50));
		unit2 = troop2;
		UnitDetector detector2 = new UnitDetector(troop2);
		detector2.Start();
		m_MapManager.VisibilityGrid.CheckVisibilityForAllSquares(troop2.Position.Value, troop2.DetectRange[DetectChannel.Visibility]);
	}

	void TestShooting()
	{
		var range = m_MapManager.MapBound.Bounds.x / 2 - 40f;
		
		UnitObservable troop1 = new UnitObservable();
		troop1.Faction = Faction.Blue;
		m_MapManager.MapGrid.AddUnitToGrid(troop1);
		m_UnitsManager.AddUnit(troop1);
		m_UnitsManager.UnitBodiesPresenter.AddUnit(troop1);
		troop1.SetNewPosition(new Vector3(40, 5, 50));
		WeaponBase weapon = new WeaponBase();
		weapon.SetOwner(troop1);
		weapon.SetProjectile(new Projectile());
		troop1.EquipWeapon(weapon);
		UnitDetector detector1 = new UnitDetector(troop1);
		detector1.Start();
		units.Add(troop1);
		
		UnitObservable troop2 = new UnitObservable();
		troop2.Faction = Faction.Red;
		m_MapManager.MapGrid.AddUnitToGrid(troop2);
		m_UnitsManager.AddUnit(troop2);
		m_UnitsManager.UnitBodiesPresenter.AddUnit(troop2);
		troop2.SetNewPosition(new Vector3(50, 5, 50));
		unit2 = troop2;
		UnitDetector detector2 = new UnitDetector(troop2);
		detector2.Start();
		
		UnitShooter unitShooter = new UnitShooter(troop1);
		unitShooter.SetTarget(unit2);
		unitShooter.Start();
	}

	void BuildUnits()
	{
//		BuildingUnitDetailsOld buildingUnitDetailsOld = new BuildingUnitDetailsOld();
//		BuildingBuilder builder = new BuildingBuilder();
//		builder.For(buildingUnitDetailsOld)
//			   .AtPosition(new Vector3(0, 0, -5))
//			   .WithScale(new Vector3(1.2f, 1.2f, 1.2f))
//			   .BuildUnit()
//			   .Print();
//		builder.Clean();
//		
//		TroopUnitBuilder troopBuilder = new TroopUnitBuilder();
//		for(int i = 0; i <= 0; i++)
//		{
//			float randomUnitScale = UnityEngine.Random.Range(1f, 2f);
//			float randomUnitPositionMultiplierX = UnityEngine.Random.Range(-5f, 5f);
//			float randomUnitPositionMultiplierZ = UnityEngine.Random.Range(-5f, 5f);
//			float randomUnitDestinationMultiplierX = UnityEngine.Random.Range(-3f, 3f);
//			float randomUnitDestinationMultiplierZ = UnityEngine.Random.Range(-3f, 3f);
//			TroopUnitDetailsOld troopUnitDetailsOld = new TroopUnitDetailsOld();
//			var troop = troopBuilder.For(troopUnitDetailsOld)
//									.WithAI<TroopUnitAI>()
//									.AtPosition(new Vector3(10 * randomUnitPositionMultiplierX, 0, 10 * randomUnitPositionMultiplierZ))
//									.WithScale(new Vector3(randomUnitScale, randomUnitScale, randomUnitScale))
//									.BuildUnit();
//			Vector3[] destinations = new Vector3[2]
//			{
//				new Vector3(2f * randomUnitDestinationMultiplierX, 0f, 2f * randomUnitDestinationMultiplierZ),
//				new Vector3(-2f * randomUnitDestinationMultiplierX, 0f, 2f * randomUnitDestinationMultiplierZ),
//				//new Vector3(5f * randomUnitDestinationMultiplierX, 0f, -11f * randomUnitDestinationMultiplierZ)
//			};
//			ComplexMoveTask complexMoveTask = new ComplexMoveTask(TaskPriority.High, destinations, troop.Body.transform);
//			complexMoveTask.OnCompleted += () => { OnComplexMoveCompleted(troop); };
//			troop.ExecuteTask(complexMoveTask);
//			troop.Print();
//			troopBuilder.Clean();
//		}
	}

	void OnComplexMoveCompleted(TroopUnit unit)
	{
		unit.DestroyUnit();
		StartCoroutine(WaitAndStart(5, BuildUnits));
	}

	IEnumerator WaitAndStart(float delay, Action Method)
	{
		yield return new WaitForSeconds(delay);
		Method?.Invoke();
	}
		
}
