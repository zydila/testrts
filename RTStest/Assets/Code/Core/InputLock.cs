﻿using Code.Core.Managers;

namespace Code.Core {
    public class InputLock
    {
        [Inject] InputManager m_InputManager;

        public InputLock()
        {
            this.Inject();
        }

        public void Unlock()
        {
            m_InputManager.UnlockInput(this);
        }
    }
}

