﻿using System;
using System.Collections;
using Code.UI;
using UnityEngine;

namespace Code.Core.Managers {
    public class ScreenManager : ManagerBase
    {
        [Inject] InputManager m_InputManager;
        [Inject] EventDispatcher m_EventDispatcher;
        
        public ScreenRoot ScreenRoot { get; private set; }
        public ScreenBase CurrentScreen { get; private set; }
        ScreenBase m_PreviousScreen;

        public void SetScreenRoot(ScreenRoot root)
        {
            ScreenRoot = root;
        }

        public ScreenManager()
        {
            Log.Info("ScreenManager.CONSTRUCT");
            FindScreenRoot();
        }

        public T OpenScreen<T>(Action onOpened = null) where T : ScreenBase
        {
            Type type = typeof(T);
            string screenPath = "UI/Prefabs/Screens/" + type.ToString();
            if (CurrentScreen != null &&  (CurrentScreen is T))
            {
                Log.Warning("Screen already Opened: " + screenPath);
                CurrentScreen.Init();
                return (T)CurrentScreen;
            }
            Log.Info("Open Screen: " + screenPath);
            RectTransform prefab = Resources.Load<RectTransform>(screenPath);
            if(prefab == null)
            {
                Log.Error("SCREEN NOT FOUND");
                return null;
            }
            T screen = ScreenRoot.SpawnScreen<T>(prefab);
            screen.Init();
            if(screen == null)
                Log.Warning("SCREEN DOESN'T HAVE SCRIPT");
            var inputLock = m_InputManager.LockInput();
            HidePreviousScreen(() =>
            {
                CurrentScreen = screen;
                CurrentScreen.ShowScreen(() => 
                {
                    inputLock.Unlock();
                    onOpened.Fire();
                });
            });
            return screen;
        }

        void HidePreviousScreen(Action onClosed)
        {
            m_PreviousScreen = CurrentScreen;
            if (m_PreviousScreen != null)
                m_PreviousScreen.HideScreen(() =>
                {
                    m_PreviousScreen.gameObject.SetActive(false);
                    onClosed.Fire();
                });
            else
                m_EventDispatcher.StartCoroutine(DelayedAction(onClosed));
        }

        IEnumerator DelayedAction(Action action)
        {
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            action.Fire();
        }

        void FindScreenRoot()
        {
            GameObject screenGO = GameObject.Find("Canvas/Screen");
            if (screenGO == null)
            {
                Log.Warning("No Screen root found");
            }
            ScreenRoot = screenGO.GetComponent<ScreenRoot>();
        }
    }
}
