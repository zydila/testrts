﻿using System;
using System.Collections.Generic;
using System.Reflection;
using JetBrains.Annotations;

namespace Code.Core.Managers {
    [AttributeUsage(AttributeTargets.Field)]
    [MeansImplicitUse(ImplicitUseKindFlags.Assign)]
    public sealed class InjectAttribute : Attribute
    {
    }

    public static class DependencyInjector
    {
        static readonly object placeholder = new object();
        static readonly List<FieldInfo> injectFieldsBuffer = new List<FieldInfo>();
        static readonly Dictionary<Type, object> components = new Dictionary<Type, object>();
        static readonly Dictionary<Type, FieldInfo[]> cachedFields = new Dictionary<Type, FieldInfo[]>();

        public static void Inject(this object target)
        {
            InjectObject(target);
        }

        public static T Resolve<T>()
        {
            return (T) Resolve(typeof(T));
        }

        public static void ReplaceComponent<T>(T newComponent)
        {
            Type type = typeof(T);

            if (components.ContainsKey(type))
            {
                if (components[type] is IDisposable cleanableComponent)
                    cleanableComponent.Dispose();
                components[type] = null;
            }

            components[type] = newComponent;

            InjectObject(newComponent);
            PostConstruct(type, newComponent);

            foreach (var c in components.Values)
            {
                foreach (var f in GetFields(c.GetType()))
                {
                    if (f.FieldType == typeof(T))
                        f.SetValue(c, newComponent);
                }
            }
        }

        public static void ClearCache()
        {
            foreach (var componentPair in components)
            {
                IDisposable cleanableComponent = componentPair.Value as IDisposable;
                if (cleanableComponent != null)
                    cleanableComponent.Dispose();
            }

            cachedFields.Clear();
            components.Clear();
            GC.Collect();
        }

        static void InjectObject(object target)
        {
            FieldInfo[] fields = GetFields(target.GetType());
            for (int i = 0; i < fields.Length; i++)
                fields[i].SetValue(target, Resolve(fields[i].FieldType));
        }

        static FieldInfo[] GetFields(Type type)
        {
            FieldInfo[] fields;
            if (!cachedFields.TryGetValue(type, out fields))
            {
                fields = GetInjectFields(type);
                cachedFields.Add(type, fields);
            }

            return fields;
        }

        static FieldInfo[] GetInjectFields(Type type)
        {
            while (type != null)
            {
                FieldInfo[] typeFields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic |
                                                        BindingFlags.Instance | BindingFlags.DeclaredOnly);
                for (int i = 0; i < typeFields.Length; i++)
                {
                    if (typeof(InjectAttribute).IsDefined(typeFields[i]))
                        injectFieldsBuffer.Add(typeFields[i]);
                }

                type = type.GetBaseType();
            }

            var resultFields = injectFieldsBuffer.ToArray();
            injectFieldsBuffer.Clear();
            return resultFields;
        }

        static object Resolve(Type type)
        {
            object component;

            if (components.TryGetValue(type, out component))
            {
                if (placeholder == component)
                    throw new Exception("Cyclic dependency detected in " + type);
            }
            else
            {
                components[type] = placeholder;
                component = components[type] = CreateComponent(type);
                (component as ManagerBase)?.Init();
                InjectObject(component);
                PostConstruct(type, component);
            }

            return component;
        }

        static void PostConstruct(Type type, object component)
        {
#if UNITY_WSA && !UNITY_EDITOR
        MethodInfo method = type.GetRuntimeMethods().FirstOrDefault(mi => mi.Name == "PostConstruct");
#else
            MethodInfo method = type.GetMethod("PostConstruct", BindingFlags.Instance | BindingFlags.NonPublic);
#endif
            if (method != null)
                method.Invoke(component, null);
        }

        static object CreateComponent(Type type)
        {
            try
            {
                return Activator.CreateInstance(type);
            }
            catch (TargetInvocationException e)
            {
                throw e.InnerException;
            }
        }
    }
}