﻿using System;
using Code.Game.Model.Units;
using UnityEngine;

namespace Code.Core.Managers {
    public class ResourcesManager : ManagerBase
    {
        public T LoadUnitBody<T>(string name) where T : UnitBody
        {
            return Resources.Load<T>($"Prefabs/Game/Units/{name}");
        }
        
        public UnitBody LoadUnitBody(string name)
        {
            return Resources.Load<UnitBody>($"Prefabs/Game/Units/{name}");
        }
    }
}
