﻿using System.Collections.Generic;
using Code.Game;
using Code.Game.Model;
using Code.Game.Model.Units;
using Code.Game.Model.Units.Interfaces;
using UniRx;
using UnityEngine;

namespace Code.Core.Managers {
    public class MapManager : ManagerBase
    {
        [Inject] PlayerManager m_PlayerManager;
        [Inject] UnitsManager m_UnitsManager;

        const float SQUARE_SIZE = 90;
        
        public MapBound MapBound { get; private set; }
        public MapGrid MapGrid { get; private set; }
        public VisibilityGrid VisibilityGrid { get; private set; }
        
        bool m_Inited;
        
        public void SetMapBound(MapBound mapBound)
        {
            MapBound = mapBound;
            InitMapGrid();
        }

        void InitMapGrid()
        {
            MapGrid = new MapGrid(MapBound.Bounds);
            VisibilityGrid = new VisibilityGrid(MapBound.Bounds);
        }

        public void OnUIMapClicked(Vector2 mouseRelativeClickPosition, RectTransform mapTransform)
        {
            Vector2 sizeDelta = mapTransform.sizeDelta;
            float mapX = sizeDelta.x / 2;
            float mapY = sizeDelta.y / 2;
            Vector2 relativeClickPosition = new Vector2((mouseRelativeClickPosition.x - mapX) * 2 / sizeDelta.x, (mouseRelativeClickPosition.y - mapY) * 2 / sizeDelta.y);
            SetPlayerPositionOnMap(relativeClickPosition);
        }

        void SetPlayerPositionOnMap(Vector2 newRelativePosition)
        {
            Vector3 newWorldLocation = new Vector3(MapBound.Bounds.x * newRelativePosition.x / 2, m_PlayerManager.CameraController.WorldCameraPosition.y, MapBound.Bounds.z * newRelativePosition.y / 2);
            m_PlayerManager.CameraController.SetCameraWorldPosition(newWorldLocation);
        }

        public Vector2 GetCurrentRelativePositionOnMap()
        {
            float x = (m_PlayerManager.CameraController.WorldCameraPosition.x + MapBound.Bounds.x / 2) / MapBound.Bounds.x;
            float y = (m_PlayerManager.CameraController.WorldCameraPosition.z + MapBound.Bounds.z / 2) / MapBound.Bounds.z;
            return new Vector2(x, y);
        }
    }
}
