﻿using System.Collections.Generic;
using UnityEngine;

namespace Code.Core.Managers {
    public class InputManager : ManagerBase
    {
        [Inject] ScreenManager m_ScreenManager;
        
        readonly HashSet<InputLock> m_InputLocks = new HashSet<InputLock>();
        
        Vector2 m_ScaledMouseInput = new Vector2();
        int m_CheckedTick;

        public InputLock LockInput()
        {
            InputLock inputLock = new InputLock();
            m_InputLocks.Add(inputLock);
            return inputLock;
        }

        public void UnlockInput(InputLock inputLock)
        {
            m_InputLocks.Remove(inputLock);
        }

        public bool IsInputLock()
        {
            return m_InputLocks.Count > 0;
        }

        public void UnlockAll()
        {
            m_InputLocks.Clear();
        }

        public Vector2 GetScaledMousePosition()
        {
            if (m_CheckedTick == Time.frameCount)
                return m_ScaledMouseInput;
            m_CheckedTick = Time.frameCount;
            float positionX = Input.mousePosition.x / Screen.width * m_ScreenManager.ScreenRoot.screenTransform.rect.width;
            float positionY = Input.mousePosition.y / Screen.height * m_ScreenManager.ScreenRoot.screenTransform.rect.height;
            m_ScaledMouseInput.Set(positionX, positionY);
            return m_ScaledMouseInput;
        }
    }
}
