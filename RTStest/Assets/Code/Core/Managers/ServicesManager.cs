﻿using System.Collections;
using System.Collections.Generic;
using Code.Core.Controllers;
using Code.Core.Managers;
using UnityEngine;

public class ServicesManager : ManagerBase
{
    public ServicesScheduler ServicesScheduler { get; private set; }

    public void SetServiceScheduler(ServicesScheduler servicesScheduler)
    {
        ServicesScheduler = servicesScheduler;
    }
}
