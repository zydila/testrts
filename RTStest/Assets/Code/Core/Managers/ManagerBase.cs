﻿using System;

namespace Code.Core.Managers {
    public class ManagerBase
    {
        public virtual void Init() { }
    }
}