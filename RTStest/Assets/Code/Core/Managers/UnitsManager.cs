﻿using System.Collections.Generic;
using Code.Game.Model;
using Code.Game.Model.Units;
using Code.Game.Model.Units.Interfaces;
using UniRx;
using UnityEngine;

namespace Code.Core.Managers {
    public class UnitsManager : ManagerBase
    {
        [Inject] ResourcesManager m_ResourcesManager;

        public UnitBodiesPresenter UnitBodiesPresenter { get; private set; }
        public UnitVisibilityChecker UnitVisibilityChecker { get; private set; }
        
        public static int UnitsCounter;
        
        UnitsPool UnitsPool;
        UnitsRoot UnitsRoot;

        public readonly Dictionary<Faction, List<IUnit>> AllUnits = new Dictionary<Faction, List<IUnit>>();
        
        public override void Init()
        {
            UnitBodiesPresenter = new UnitBodiesPresenter();
            UnitVisibilityChecker = new UnitVisibilityChecker();
        }

        public void AddUnit(IUnit unit)
        {
            CheckFaction(unit.Faction);
            if (!AllUnits[unit.Faction].Contains(unit))
                AllUnits[unit.Faction].Add(unit);
            unit.Destroy.DoOnCompleted(() => RemoveUnit(unit));
            unit.Destroyed.Subscribe((destroyed) =>
            {
                if (destroyed) 
                    RemoveUnit(unit); //TODO: think about;
            });
        }

        public List<IUnit> GetUnitsOfFaction(Faction faction)
        {
            CheckFaction(faction);
            return AllUnits[faction];
        }

        public void RemoveUnit(IUnit unit)
        {
            CheckFaction(unit.Faction);
            AllUnits[unit.Faction].Remove(unit);
        }

        void CheckFaction(Faction faction)
        {
            if (!AllUnits.ContainsKey(faction))
                AllUnits.Add(faction, new List<IUnit>());
        }

        public void SetUnitsPool(UnitsPool pool)
        {
            UnitsPool = pool;
        }
        
        public void SetUnitRoot(UnitsRoot root)
        {
            UnitsRoot = root;
        }

        public UnitBody GetUnitBody(string desiredBody)
        {
            UnitBody resultBody = null;
            resultBody = UnitsPool.TryRetrieveBodyFromPool(desiredBody, out resultBody) 
                       ? resultBody 
                       : UnitsRoot.TryCopyExistingBody(desiredBody, out resultBody) ? resultBody : CreateBody(desiredBody);
            if (resultBody != null)
                UnitsRoot.Add(resultBody);
            return resultBody;
        }

        public bool AddBodyToPool(UnitBody body)
        {
            UnitsRoot.Remove(body);
            return UnitsPool.Add(body);
        }

        public void MoveAllBodiesToPool()
        {
            foreach (UnitBody unitBody in UnitsRoot.UnitBodies)
                UnitsPool.Add(unitBody);
            UnitsRoot.ClearRoot();
        }

        public void MoveBodiesToPool(List<UnitBody> bodies)
        {
            foreach (UnitBody unitBody in bodies)
            {
                UnitsRoot.Remove(unitBody);
                UnitsPool.Add(unitBody);
            }
        }

        UnitBody CreateBody(string desiredBody)
        { 
            UnitsCounter++;
            return Object.Instantiate(m_ResourcesManager.LoadUnitBody(desiredBody));
        }
    }
}
