﻿using System;
using System.Collections;
using UnityEngine;

namespace Code.Core.Managers {
    public class EventDispatcher : ManagerBase
    {
        public event Action OnUpdate;
        public event Action OnFixedUpdate;

        EventDispatcherBehavior behavior;

        public void SetEventDispatcherBehavior(EventDispatcherBehavior eventDispatcherBehavior)
        {
            behavior = eventDispatcherBehavior;
            eventDispatcherBehavior.Dispatcher = this;
        }

        public void StartCoroutine(IEnumerator coroutine)
        {
            behavior.StartCoroutine(coroutine);
        }

        public class InternalDispatcher : MonoBehaviour
        {
            EventDispatcher dispatcher;
            public EventDispatcher Dispatcher
            {
                get
                {
                    if (dispatcher == null)
                        dispatcher = DependencyInjector.Resolve<EventDispatcher>();
                    return dispatcher;
                }
                set
                {
                    dispatcher = value;
                }
            }
        
            void Update()
            {
                Dispatcher?.OnUpdate?.Invoke();
            }
        
            void FixedUpdate()
            {
                Dispatcher?.OnFixedUpdate?.Invoke();
            }
        }
    }
}
