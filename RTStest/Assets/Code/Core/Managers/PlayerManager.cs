﻿using System;
using Code.Game.Model.Player;

namespace Code.Core.Managers {
    public class PlayerManager : ManagerBase
    {
        public CameraController CameraController { get; private set; }

        public void SetCameraController(CameraController cameraController)
        {
            CameraController = cameraController;
        }
    }
}