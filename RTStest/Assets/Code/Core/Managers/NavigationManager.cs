﻿using Code.Game.Model.Navigation;
using Code.Game.Model.Units;
using UnityEngine;
using UnityEngine.AI;

namespace Code.Core.Managers
{
    public class NavigationManager : ManagerBase
    {
        public NavigationController NavigationController { get; private set; }

        NavMeshHit m_NavMeshHit;

        public void SetNavigationController(NavigationController navController)
        {
            NavigationController = navController;
        }

        public bool GetClosestNavPoint(Vector3 point, UnitElement element, out Vector3 closestPointOnNavMesh)
        {
            return NavigationController.GetClosestNavPoint(point, element, out closestPointOnNavMesh);
        }
    }
}