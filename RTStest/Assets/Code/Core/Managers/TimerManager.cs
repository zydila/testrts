﻿using System;
using Code.Core.Controllers;

namespace Code.Core.Managers {
    public class TimerManager : ManagerBase
    {
        readonly DateTime m_RefDateTime = new DateTime(2010, 1, 1);
        public long CurrentTimestamp { get { return (long)(DateTime.Now - m_RefDateTime).TotalSeconds; } }
        
        GlobalTimerController m_TimerController;
    
        public void SetGlobalTimerController(GlobalTimerController controller)
        {
            m_TimerController = controller;
        }

        public Timer StartTimer(int timeActive, bool repeatingTimer, Action onTimerFinished)
        {
            Timer timer = new Timer(timeActive, repeatingTimer, onTimerFinished);
            m_TimerController.StartTimer(timer);
            return timer;
        }

        public Timer StartTimer(Timer timer)
        {
            m_TimerController.StartTimer(timer);
            return timer;
        }
    }
}
