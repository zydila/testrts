﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Reflection;
using Code.Core;
using Code.Core.Common;

public static class BaseExtentions
{
    public static string Localize(this string textKey)
    {
        return Language.GetLocalizationByKey(textKey);
    }

    public static string LocalizeFormat(this string textKey, params object[] args)
    {
        return string.Format(textKey.Localize(), args);
    }
    
    public static bool IsSubclassOf(this Type toCheck, Type generic) 
    {
        while (toCheck != null && toCheck != typeof(object)) {
            var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
            if (generic == cur) {
                return true;
            }
            toCheck = toCheck.BaseType;
        }
        return false;
    }

    public static Type GetBaseType(this Type type)
    {
        return type.BaseType;
    }
    
    public static bool IsDefined(this Type attribute, MemberInfo m)
    {
        return m.GetCustomAttributes(attribute, true).Any();
    }
    
    /// <summary>
    /// COPYPASTE
    /// </summary>
    public static void Fire(this Action action)
    {
        if (action == null)
            return;
        Delegate[] delegates = action.GetInvocationList();
        for (int index = 0; index < delegates.Length; ++index)
        { Action delegateAction = (Action)delegates[index];
            if (IsSafeDelegate(delegateAction))
                delegateAction();
        }
    }

    /// <summary>
    /// COPYPASTE
    /// </summary>
    public static void Fire<T>(this Action<T> action, T param)
    {
        if (action == null)
            return;
        Delegate[] delegates = action.GetInvocationList();
        for (int index = 0; index < delegates.Length; ++index)
        {
            Action<T> delegateAction = (Action<T>)delegates[index];
            if (IsSafeDelegate(delegateAction))
                delegateAction(param);
        }
    }

    /// <summary>
    /// COPYPASTE
    /// </summary>
    public static void Fire<T1, T2>(this Action<T1, T2> action, T1 param1, T2 param2)
    {
        if (action == null)
            return;
        Delegate[] delegates = action.GetInvocationList();
        for (int index = 0; index < delegates.Length; ++index)
        {
            Action<T1, T2> delegateAction = (Action<T1, T2>)delegates[index];
            if (IsSafeDelegate(delegateAction))
                delegateAction(param1, param2);
        }
    }

    /// <summary>
    /// COPYPASTE
    /// </summary>
    static bool IsSafeDelegate(Delegate del)
    {
        object target = del.Target;
        if (target == null)
        {
            if (!del.Method.IsStatic)
            {
                Log.Warning(string.Format("Delegate target is null along with not static method: {0} {1}", del.Method.Name, del.Method.DeclaringType));
                return false;
            }
        }
        else if ((target is UnityEngine.Object) && target.Equals(null))
        {
            Log.Warning(string.Format("Delegate target was destroyed in native side but exist managed object: {0} {1}\n{2}", del.Method.Name, del.Method.DeclaringType, StackTraceUtility.ExtractStackTrace()));
            return false;
        }
        return true;
    }

    public static SimpleVector2 Abs(this SimpleVector2 vector2)
    {
        return new SimpleVector2(Mathf.Abs(vector2.X), Mathf.Abs(vector2.Y));
    }

    public static SimpleVector2Int Abs(this SimpleVector2Int vector2)
    {
        return new SimpleVector2Int(Mathf.Abs(vector2.X), Mathf.Abs(vector2.Y));
    }

    public static Vector2 Abs(this Vector2 vector2)
    {
        vector2.Set(Mathf.Abs(vector2.x), Mathf.Abs(vector2.y));
        return vector2;
        return new Vector2(Mathf.Abs(vector2.x), Mathf.Abs(vector2.y));
    }

    public static Vector3 Abs(this Vector3 vector2)
    {
        return new Vector3(Mathf.Abs(vector2.x), Mathf.Abs(vector2.y), Mathf.Abs(vector2.z));
    }
}
