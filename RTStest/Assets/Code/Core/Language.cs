﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Code.Core {
    public class Language
    {
        public enum Languages
        {
            None = -1,
            English = 0,
            Russian = 1,
            Estonian = 2
        }

        public static event Action OnLanguageChanged;

        static Language.Languages currentLanguage = Languages.None;
        public static Language.Languages CurrentLanguage
        {
            get
            {
                if (currentLanguage == Languages.None)
                    currentLanguage = GetCurrentLanguage();
                return currentLanguage;
            }
        }

        static Dictionary<string, string> localization = new Dictionary<string, string>();

        static Dictionary<Languages, string> LanguageFilesPaths = new Dictionary<Languages, string>
        {
            { Languages.English, "Assets/Resources/UI/Localization/En_Loc.txt"},
            { Languages.Russian, "Assets/Resources/UI/Localization/Ru_Loc.txt"},
            { Languages.Estonian, "Assets/Resources/UI/Localization/Ee_Loc.txt"}
        };

        public static string GetLocalizationByKey(string key)
        {
            if (localization == null || localization.Count == 0)
                LoadCurrentLocalization();
            string localizedText;
            localization.TryGetValue(key, out localizedText);
            return localizedText;
        }

        public static string GetLanguageFilePath(Languages language)
        {
            string filePath;
            if (!LanguageFilesPaths.TryGetValue(language, out filePath))
                return null;
            return filePath;
        }

        public static void ChangeLanguage(Language.Languages language)
        {
            if (currentLanguage == language)
                return;
            localization = LoadLocalization(GetLanguageFilePath(language));
            SaveLanguage(language);
            OnLanguageChanged.Fire();
        }

        public static Language.Languages GetCurrentLanguage()
        {
            var language = (Language.Languages)PlayerPrefs.GetInt("language");
            if (language == Languages.None)
                ChangeLanguage(Languages.English);
            return language;
        }

        public static void LoadCurrentLocalization()
        {
            ChangeLanguage(GetCurrentLanguage());
        }

        static void SaveLanguage(Language.Languages language)
        {
            currentLanguage = language;
            PlayerPrefs.SetInt("language", (int)language);
            PlayerPrefs.Save();
        }

        static Dictionary<string, string> LoadLocalization(string path)
        {
            if (!File.Exists(path))
                return null;

            using (var reader = new StreamReader(new FileStream(path, FileMode.Open)))
                return LoadLocalization(reader);
        }

        static Dictionary<string, string> LoadLocalization(TextReader reader)
        {
            var result = new Dictionary<string, string>();
            int entry = 0;
            do
            {
                string key = reader.ReadLine();
                string val = reader.ReadLine();

                if (key == null || val == null)
                    break;

                result[UnescapeSymbols(key)] = UnescapeSymbols(val);
                entry++;
            }
            while (true);
            return result;
        }

        static string UnescapeSymbols(string str)
        {
            return str.Replace("\\n", "\n");
        }
    }
}