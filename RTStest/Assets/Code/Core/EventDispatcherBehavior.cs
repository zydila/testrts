﻿using Code.Core.Managers;

namespace Code.Core {
    public class EventDispatcherBehavior : EventDispatcher.InternalDispatcher
    {
        void Start()
        {
            DependencyInjector.Resolve<EventDispatcher>().SetEventDispatcherBehavior(this);
        }
    }
}
