﻿using Newtonsoft.Json;
using UnityEngine;

namespace Code.Core.Common {
    public static class CommonTools
    {
    }

    public struct SimpleVector2
    {
        public float X;
        public float Y;

        [JsonIgnore]
        public float Magnitude
        {
            get { return Mathf.Sqrt(Mathf.Pow(X, 2) + Mathf.Pow(Y, 2)); }
        }

        public SimpleVector2(float x, float y)
        {
            X = x;
            Y = y;
        }
        
        public static SimpleVector2 operator +(SimpleVector2 a, SimpleVector2 b)
            => new SimpleVector2(a.X + b.X, a.Y + b.Y);
        
        public static SimpleVector2 operator -(SimpleVector2 a, SimpleVector2 b)
            => new SimpleVector2(a.X - b.X, a.Y - b.Y);
        
        public static bool operator ==(SimpleVector2 a, SimpleVector2 b)
            => Mathf.Approximately(a.X, b.X) && Mathf.Approximately(a.Y, b.Y);

        public static bool operator !=(SimpleVector2 a, SimpleVector2 b)
            => !Mathf.Approximately(a.X, b.X) || !Mathf.Approximately(a.Y, b.Y);
    }
    
    public struct SimpleVector2Int
    {
        public int X;
        public int Y;

        [JsonIgnore]
        public float Magnitude
        {
            get { return Mathf.Sqrt(Mathf.Pow(X, 2) + Mathf.Pow(Y, 2)); }
        }

        public SimpleVector2Int(int x, int y)
        {
            X = x;
            Y = y;
        }
        
        public static SimpleVector2 operator +(SimpleVector2Int a, SimpleVector2Int b)
            => new SimpleVector2(a.X + b.X, a.Y + b.Y);
        
        public static SimpleVector2 operator -(SimpleVector2Int a, SimpleVector2Int b)
            => new SimpleVector2(a.X - b.X, a.Y - b.Y);
        
        public static bool operator ==(SimpleVector2Int a, SimpleVector2Int b)
            => a.X == b.X && a.Y == b.Y;
        
        public static bool operator !=(SimpleVector2Int a, SimpleVector2Int b)
            => a.X != b.X || a.Y != b.Y;
    }
}
