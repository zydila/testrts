﻿namespace Code.Core {
    public static class GlobalConstants
    {
        public const int SCENE_SELECTABLE_LAYER = 9;
        public const int VISIBILITY_LAYER = 11;
        public const int WATER_LAYER = 12;
        public const int TERRAIN_LAYER = 10;
        
        public const int VISIBILITY_TERRAIN_AND_WATER_MASK = (1 << TERRAIN_LAYER) | (1 << WATER_LAYER);
        
        public const int NAVMESH_GROUND_ROAD_AREA = 5;
        public const int NAVMESH_GROUND_AREA = 6;
        public const int NAVMESH_WATER_AREA = 4;
        
        public const int TARGET_FRAMERATE = 60;
    }
}
