﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Code.Core.Common;
using Code.Game;
using Code.Game.Model;
using Newtonsoft.Json;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Code.Core.Tools {
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    public class VisibilityGridGenerator : MonoBehaviour
    {
        [SerializeField] MapBound mapBound;
        
        const int VISIBILITY_MASK = GlobalConstants.VISIBILITY_TERRAIN_AND_WATER_MASK | (1 << GlobalConstants.VISIBILITY_LAYER);

        static VisibilitySquareDTO[,] m_VisibilitySquaresData;
        
        int m_SquaresRows;
        int m_SquaresColumns;
        bool m_Inited;

        float m_StartX;
        float m_StartY;

        int m_VisibilityRadiusInSquares;
        
        RaycastHit m_HitInfo;
        
        public void Generate()
        {
            Log.Info("VisibilityGrid.VISIBILITY_SQUARE_SIZE = " + VisibilityGrid.VISIBILITY_CELL_SIZE);
            Log.Info("VisibilityGrid.MAXIMAL_VISIBILITY_DETECTION_RADIUS = " + VisibilityGrid.MAXIMAL_VISIBILITY_DETECTION_RADIUS);

            BuildVisibilitySquares();
            BuildVisibilityForSquare(ref m_VisibilitySquaresData[50,50]);
            WriteToFile();
        }

        void BuildVisibilitySquares()
        {
            m_VisibilityRadiusInSquares = Mathf.CeilToInt(VisibilityGrid.MAXIMAL_VISIBILITY_DETECTION_RADIUS / VisibilityGrid.VISIBILITY_CELL_SIZE);
            m_StartX = -mapBound.Bounds.x / 2 + VisibilityGrid.VISIBILITY_CELL_SIZE / 2;
            m_StartY = -mapBound.Bounds.z / 2 + VisibilityGrid.VISIBILITY_CELL_SIZE / 2;
            m_SquaresRows = Mathf.CeilToInt(mapBound.Bounds.x / VisibilityGrid.VISIBILITY_CELL_SIZE);
            m_SquaresColumns = Mathf.CeilToInt(mapBound.Bounds.z / VisibilityGrid.VISIBILITY_CELL_SIZE);
            m_VisibilitySquaresData = new VisibilitySquareDTO[m_SquaresRows, m_SquaresColumns];
            for (int i = 0; i < m_SquaresRows; i++)
                for (int j = 0; j < m_SquaresColumns; j++)
                    m_VisibilitySquaresData[i, j] = new VisibilitySquareDTO(i, j, AdjustToTerrain(new Vector3(m_StartX + VisibilityGrid.VISIBILITY_CELL_SIZE * i, mapBound.Bounds.y,m_StartY + VisibilityGrid.VISIBILITY_CELL_SIZE * j))); 
        }

        void WriteToFile()
        {
            StringBuilder visibilitySave = new StringBuilder();
            foreach (VisibilitySquareDTO squareDto in m_VisibilitySquaresData)
            {
                visibilitySave.Append(squareDto.Height);
                visibilitySave.Append(VisibilityGrid.VISIBILITY_GRID_SAVE_DELIMITER);
            }
            File.WriteAllText(string.Format(VisibilityGrid.VISIBILITY_GRID_SAVE_FILE, EditorSceneManager.GetActiveScene().name), visibilitySave.ToString());
        }

        Vector3 AdjustToTerrain(Vector3 vector)
        {
            if (Physics.Raycast(vector, Vector3.down, out m_HitInfo, mapBound.Bounds.y * 2, VISIBILITY_MASK))
                vector = m_HitInfo.point + new Vector3(0, 2f, 0);
            return vector;
        }

        public struct VisibilitySquareDTO
        {
            [JsonIgnore] public Vector2 CenterV2;
            [JsonIgnore] public Vector3 AdjustedWorldCenterV3;

            [JsonIgnore] public readonly int X;
            [JsonIgnore] public readonly int Y;
            
            [JsonIgnore] readonly Vector3 m_LeftBottomWorld;
            [JsonIgnore] readonly Vector3 m_LeftUpWorld;
            [JsonIgnore] readonly Vector3 m_RightUpWorld;
            [JsonIgnore] readonly Vector3 m_RightBottomWorld;

            [JsonProperty("height")] public float Height;

            public VisibilitySquareDTO(int indexX, int indexY, Vector3 adjustedCenter)
            {
                X = indexX;
                Y = indexY;
                CenterV2 = new Vector2(X, Y);
                
                AdjustedWorldCenterV3 = adjustedCenter;
                Height = adjustedCenter.y;
                
                m_LeftBottomWorld = AdjustedWorldCenterV3 + new Vector3(-0.49f, 0, -0.49f) * VisibilityGrid.VISIBILITY_CELL_SIZE;
                m_LeftUpWorld = AdjustedWorldCenterV3 + new Vector3(-0.5f, 0, 0.49f) * VisibilityGrid.VISIBILITY_CELL_SIZE;
                m_RightUpWorld = AdjustedWorldCenterV3 + new Vector3(0.49f, 0, 0.49f) * VisibilityGrid.VISIBILITY_CELL_SIZE;
                m_RightBottomWorld = AdjustedWorldCenterV3 + new Vector3(0.49f, 0, -0.49f) * VisibilityGrid.VISIBILITY_CELL_SIZE;
            }

            public void Draw(Color color)
            {
                Debug.DrawLine(m_LeftBottomWorld, m_LeftUpWorld, color, 5f);
                Debug.DrawLine(m_LeftUpWorld, m_RightUpWorld, color, 5f);
                Debug.DrawLine(m_RightUpWorld, m_RightBottomWorld, color, 5f);
                Debug.DrawLine(m_RightBottomWorld, m_LeftBottomWorld, color, 5f);
            }
        }

        void BuildVisibilityForSquare(ref VisibilitySquareDTO square)
        {
            square.Draw(Color.white);
            for(int n = 1; n < m_VisibilityRadiusInSquares; n++)
                for(int i = -n; i <= n; i++)
                    for (int j = -n; j <= n; j++)
                    {
                        if (Mathf.Abs(i) < n && Mathf.Abs(j) < n)
                            continue;
                        int checkingX = square.X + i;
                        int checkingY = square.Y + j;
                        if (checkingX < 0 || checkingX >= m_SquaresRows || checkingY < 0 || checkingY >= m_SquaresColumns)
                            continue;
                        if (CheckVisibilityBlockedByOtherSquaresTest(square, m_VisibilitySquaresData[checkingX, checkingY]))
                            m_VisibilitySquaresData[checkingX, checkingY].Draw(Color.yellow);
                        else
                            m_VisibilitySquaresData[checkingX, checkingY].Draw(Color.blue);
                    }
        }
        
        bool CheckVisibilityBlockedByOtherSquaresTest(VisibilitySquareDTO from, VisibilitySquareDTO to)
        {
            if (from.X == to.X && from.Y == to.Y)
                return false;
            bool centerBlocked = false;
            bool cornerLeftBlocked = false;
            bool cornerRightBlocked = false;
            
            int indexX = to.X - from.X;
            int indexY = to.Y - from.Y;
            int indexXSign = indexX < 0 ? -1 : 1;
            int indexYSign = indexY < 0 ? -1 : 1;
            
            indexX = Mathf.Abs(indexX);
            indexY = Mathf.Abs(indexY);

            Vector2 fromRelative = (to.CenterV2 - from.CenterV2).Abs();
            Vector2 directionLeft = (fromRelative + new Vector2(-0.5f, 0.5f)).normalized;
            Vector2 directionRight = (fromRelative + new Vector2(0.5f, -0.5f)).normalized;
            if (directionRight.y < 0)
                directionRight.x = 1;
            Vector2 directionCenter = fromRelative.normalized;
            for(int i = 0; i <= indexX; i++)
            {
                for (int j = 0; j <= indexY; j++)
                {
                    if (i == 0 && j == 0)
                        continue;
                    int checkingBlockerIndexX = from.X + indexXSign * i;
                    int checkingBlockerIndexY = from.Y + indexYSign * j;

                    Vector2 blockerRelative = (m_VisibilitySquaresData[checkingBlockerIndexX, checkingBlockerIndexY].CenterV2 - from.CenterV2).Abs();
                    Vector2 directionBlockerLeftUp = (blockerRelative + new Vector2(-0.5f, 0.5f)).normalized;
                    Vector2 directionBlockerRightBottom = (blockerRelative + new Vector2(0.5f, -0.5f)).normalized;
                    
                    if (directionBlockerRightBottom.y < 0)
                        directionBlockerRightBottom.x = 1.1f;
                    Vector3 normalizedDirectionBlocker = (m_VisibilitySquaresData[checkingBlockerIndexX, checkingBlockerIndexY].AdjustedWorldCenterV3 - from.AdjustedWorldCenterV3).normalized;
                    Vector3 normalizedDirection = (to.AdjustedWorldCenterV3 - from.AdjustedWorldCenterV3).normalized;
                    
                    if (normalizedDirectionBlocker.y < normalizedDirection.y || Mathf.Abs(normalizedDirectionBlocker.y - normalizedDirection.y) < 0.01f)
                        continue;

                    if (!cornerLeftBlocked && (
                                                  (Mathf.Approximately(directionLeft.x, directionBlockerLeftUp.x) &&
                                                   Mathf.Approximately(directionLeft.y, directionBlockerLeftUp.y)) ||
                                                  (Mathf.Approximately(directionLeft.x, directionBlockerRightBottom.x) &&
                                                   Mathf.Approximately(directionLeft.y, directionBlockerRightBottom.y))))
                        cornerLeftBlocked = true;

                    if (!cornerRightBlocked && (
                                                   (Mathf.Approximately(directionRight.x, directionBlockerLeftUp.x) &&
                                                    Mathf.Approximately(directionRight.y, directionBlockerLeftUp.y)) ||
                                                   (Mathf.Approximately(directionRight.x, directionBlockerLeftUp.x) &&
                                                    Mathf.Approximately(directionRight.y, directionBlockerLeftUp.y))))
                        cornerRightBlocked = true;

                    if (!centerBlocked && (
                                              (Mathf.Approximately(directionCenter.x, directionBlockerLeftUp.x) &&
                                               Mathf.Approximately(directionCenter.y, directionBlockerLeftUp.y)) ||
                                              (Mathf.Approximately(directionCenter.x, directionBlockerLeftUp.x) &&
                                               Mathf.Approximately(directionCenter.y, directionBlockerLeftUp.y))))
                        centerBlocked = true;

                    if (!cornerLeftBlocked &&
                        directionLeft.x > directionBlockerLeftUp.x &&
                        directionLeft.x < directionBlockerRightBottom.x)
                        cornerLeftBlocked = true;

                    if (!cornerRightBlocked &&
                        directionRight.x < directionBlockerRightBottom.x &&
                        directionRight.x > directionBlockerLeftUp.x)
                        cornerRightBlocked = true;

                    if (!centerBlocked &&
                        directionCenter.x < directionBlockerRightBottom.x &&
                        directionCenter.x > directionBlockerLeftUp.x)
                        centerBlocked = true;

                    if (cornerLeftBlocked && centerBlocked && cornerRightBlocked)
                        return true;
                }
            }
            return false;
        }
        
    }
}
