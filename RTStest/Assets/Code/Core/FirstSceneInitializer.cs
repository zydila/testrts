﻿using System.Collections;
using System.Collections.Generic;
using Code.Core;
using UnityEngine;

public class FirstSceneInitializer : MonoBehaviour
{
    void Start ()
    {
        Init();
	}

    void Init()
    {
        Language.LoadCurrentLocalization();
    }
}
