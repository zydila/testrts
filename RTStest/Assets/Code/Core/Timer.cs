﻿using System;
using System.Collections.Generic;
using Code.Core.Managers;

namespace Code.Core {
    public class Timer
    {
        [Inject] TimerManager m_TimerManager;
        
        public long EndTimestamp { get; private set; }

        public long TimeLeft
        {
            get
            {
                if (m_IsFinished)
                    return 0;
                return EndTimestamp - m_TimerManager.CurrentTimestamp;
            }
        }

        public bool RepeatingTimer { get; }
        public readonly Action OnTimePassed;
        
        readonly int m_Time;
        bool m_IsFinished;
        
//        public Timer(int endTimestamp, Action onTimerFinished)
//        {
//            EndTimestamp = endTimestamp;
//            OnTimerFinished = onTimerFinished;
//        }
        
        public Timer(int time, bool repeatingTimer, Action onTimePassed)
        {
            this.Inject();
            m_Time = time;
            EndTimestamp = m_TimerManager.CurrentTimestamp + time;
            RepeatingTimer = repeatingTimer;
            OnTimePassed = onTimePassed;
        }

        public void ExtendTimer(int extendTime)
        {
            if (m_IsFinished)
            {
                Log.Warning("Not possible to extend, timer finished");
                return;
            }
            EndTimestamp += extendTime;
        }

        public void Finish()
        {
            if (m_IsFinished)
                return;
            m_IsFinished = !RepeatingTimer;
            if (RepeatingTimer)
                ExtendTimer(m_Time);
            OnTimePassed?.Invoke();
        }
    }
}
