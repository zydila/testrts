﻿using System;
using System.Collections.Generic;
using Code.Core.Managers;
using UnityEngine;

namespace Code.Core.Controllers {
    public class GlobalTimerController : MonoBehaviour
    {
        [Inject] TimerManager m_TimerManager;

        readonly List<Timer> m_Timers = new List<Timer>();
        long m_CheckedSecond = 0;
    
        void Awake()
        {
            this.Inject();
            m_TimerManager.SetGlobalTimerController(this);
        }

        public void StartTimer(Timer timer)
        {
            if (CheckTimerFinished(timer, m_TimerManager.CurrentTimestamp))
            {
                timer.Finish();
                return;
            }
            m_Timers.Add(timer);
        }

        void FixedUpdate()
        {
            if (m_Timers.Count == 0)
                return;
            long currentSeconds = m_TimerManager.CurrentTimestamp;
            if (m_CheckedSecond == currentSeconds)
                return;
            m_CheckedSecond = currentSeconds;
            foreach (Timer timer in m_Timers)
            {
                if (CheckTimerFinished(timer, currentSeconds))
                {
                    timer.Finish();
                    if (!timer.RepeatingTimer)
                        m_Timers.Remove(timer);
                }
            }
        }

        bool CheckTimerFinished(Timer timer, long currentTimestamp)
        {
            return currentTimestamp >= timer.EndTimestamp;
        }
    }
}
