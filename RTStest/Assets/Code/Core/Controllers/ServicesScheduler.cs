﻿using UniRx;
using System.Collections.Generic;
using Code.Core.Managers;
using Code.Game.Model.Services;
using Code.Game.Model.Units;
using Code.Game.Model.Units.Interfaces;
using Code.Game.Services;
using UnityEngine;

namespace Code.Core.Controllers {
    public class ServicesScheduler : MonoBehaviour
    {
        [Inject] ServicesManager m_ServicesManager;
        [Inject] UnitsManager m_UnitsManager;
        
        const int DETECTOR_SERVICES_EXECUTION_LIMIT_PER_FRAME = 10;
        const int MOVER_SERVICES_EXECUTION_LIMIT_PER_FRAME = 300;
        const int SHOOTER_SERVICES_EXECUTION_LIMIT_PER_FRAME = 300;
        
        readonly List<UnitDetector> m_ActiveDetectorServices = new List<UnitDetector>();
        readonly List<UnitDetector> m_DetectorServices = new List<UnitDetector>();
        int currentDetectorIndex;
        int m_SmoothDetectorsPerFrame;
        
        readonly List<UnitMover> m_ActiveSquareMoverServices = new List<UnitMover>();
        readonly List<UnitMover> m_MoverServices = new List<UnitMover>();
        int currentMoverIndex;
        int m_SmoothMoversPerFrame;
        
        readonly List<UnitShooter> m_ActiveSquareShootersServices = new List<UnitShooter>();
        readonly List<UnitShooter> m_ShootersServices = new List<UnitShooter>();
        int currentShooterIndex;
        int m_SmoothShootersPerFrame;
        

        void Awake()
        {
            this.Inject();
            m_ServicesManager.SetServiceScheduler(this);
        }

        public void Add(IService service)
        {
            switch (service) 
            {
                case UnitDetector unitDetector:
                    m_DetectorServices.Add(unitDetector);
                    unitDetector.InPlayerView.Subscribe(inView =>
                    {
                        if (inView)
                            m_ActiveDetectorServices.Add(unitDetector);
                        else
                            m_ActiveDetectorServices.Remove(unitDetector);
                    });
                    break;
                case UnitMover unitMover:
                    m_MoverServices.Add(unitMover);
                    unitMover.InPlayerView.Subscribe(active =>
                    {
                        if (active)
                            m_ActiveSquareMoverServices.Add(unitMover);
                        else
                            m_ActiveSquareMoverServices.Remove(unitMover);
                    });
                    break;
                case UnitShooter unitShooter:
                    m_ShootersServices.Add(unitShooter);
                    unitShooter.InPlayerView.Subscribe(active =>
                    {
                        if (active)
                            m_ActiveSquareShootersServices.Add(unitShooter);
                        else
                            m_ActiveSquareShootersServices.Remove(unitShooter);
                    });
                    break;
            }
        }

        public void Remove(IService service)
        {
            if (service is UnitDetector unitDetector)
            {
                m_ActiveDetectorServices.Remove(unitDetector);
                m_DetectorServices.Remove(unitDetector);
                return;
            }
            if (service is UnitMover unitMover)
            {
                m_ActiveSquareMoverServices.Remove(unitMover);
                m_MoverServices.Remove(unitMover);
                return;
            }
            if (service is UnitShooter unitShooter)
            {
                m_ActiveSquareShootersServices.Remove(unitShooter);
                m_ShootersServices.Remove(unitShooter);
                return;
            }
        }

        void Update()
        {
            //forget all units, debug TODO: make actual forget system
            foreach (KeyValuePair<Faction,List<IUnit>> units in m_UnitsManager.AllUnits)
                foreach (IUnit unit in units.Value)
                    if (unit is IDetectable)
                        if (Time.timeSinceLevelLoad - unit.LastDetectedTime > 3f)
                            unit.Forget();
            ExecuteShooters();
            ExecuteMovers();
            ExecuteDetectors();
        }

        void ExecuteDetectors()
        {
            ExecuteServices(m_ActiveDetectorServices, m_DetectorServices, ref currentDetectorIndex, DETECTOR_SERVICES_EXECUTION_LIMIT_PER_FRAME);
        }

        void ExecuteMovers()
        {
            ExecuteServices(m_ActiveSquareMoverServices, m_MoverServices, ref currentMoverIndex, MOVER_SERVICES_EXECUTION_LIMIT_PER_FRAME);
        }
        
        void ExecuteShooters()
        {
            ExecuteServices(m_ActiveSquareShootersServices, m_ShootersServices, ref currentShooterIndex, SHOOTER_SERVICES_EXECUTION_LIMIT_PER_FRAME);
        }

        void ExecuteServices<T>(List<T> activeServices, List<T> allServices, ref int index, int limit) where T : IService 
        {
            T[] activeServicesCopy = new T[activeServices.Count]; 
            activeServices.CopyTo(activeServicesCopy);
            foreach (T activeService in activeServicesCopy)
                activeService.Execute();
            if (allServices.Count == 0)
                return;
            if (index >= allServices.Count)
                index = 0;
            int checkLimit = Mathf.Min((allServices.Count - activeServices.Count) / GlobalConstants.TARGET_FRAMERATE, limit);
            checkLimit = Mathf.Max(checkLimit, 1);
            for(int i = 0; i < checkLimit; i++)
            {
                if (allServices[index].InPlayerView.Value)
                {
                    i--;
                    index++;
                    if (index >= allServices.Count)
                        return;
                    continue;
                }
                if (!allServices[index].Execute())
                    i--;
                index++;
                if (index >= allServices.Count)
                    return;
            }
        }
    }
}
