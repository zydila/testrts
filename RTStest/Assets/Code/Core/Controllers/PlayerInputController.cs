﻿using Code.Core.Managers;
using Code.Game.Model.Units;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Code.Core.Controllers {
    public class PlayerInputController : MonoBehaviour
    {
        [Inject] InputManager m_InputManager;
        [Inject] PlayerManager m_PlayerManager;

        [SerializeField] GameObject uiGo;
        [SerializeField] EventSystem eventSystem;

        const int FRACTION_OF_SCREEN_THAT_TRIGGERS_CAMERA_MOVE = 5;
        
        GraphicRaycaster m_Raycaster;
        bool m_Mouse1Pressed;
        bool m_Mouse2Pressed;
        PointerEventData m_PointerEventData;

        bool m_CacheUiOnTheWay;
        int m_FrameUiChecked;
        Vector3 m_MousePositionCache;

        void Awake()
        {
            this.Inject();
        }

        void Update()
        {
            if (m_InputManager.IsInputLock())
            {
                m_Mouse1Pressed = false;
                m_Mouse2Pressed = false;
                return;
            }
            if (!Input.mousePresent) 
                return;
            ProcessMouseInput();
            
            HandleCameraRotation();
            HandleCameraMove();
            HandleCameraHeight();
        }

        bool ProcessMouseInput()
        {
            if (Input.GetButtonDown("Fire1"))
            {
                if (m_Mouse1Pressed)
                    return true;
            }
            else
            {
                m_Mouse1Pressed = false;
                return false;
            }
            m_Mouse1Pressed = true;
            DetectHit(Input.mousePosition);
            return true;
        }

        bool DetectHit(Vector2 position)
        {
            if (UIOnTheWay(position))
                return false;
            Ray ray = m_PlayerManager.CameraController.MainCamera.ScreenPointToRay(position);
            if (Physics.Raycast(ray, out RaycastHit hit) && hit.collider != null)
            {
                ISceneClickable hitTarget = hit.collider.GetComponent<ISceneClickable>();
                if (hitTarget == null || !hitTarget.CheckLayer(GlobalConstants.SCENE_SELECTABLE_LAYER))
                    return false;
                hitTarget.OnClicked();
                return true;
            }

            return false;
        }

        bool UIOnTheWay(Vector2 position)
        {
            return EventSystem.current.IsPointerOverGameObject();
        }

        void HandleCameraMove()
        {
            if (!IsMouseOnScreen() || Input.GetButton("Fire2"))
                return;
            
            int moveTriggerScreenDistanceX = Screen.width / FRACTION_OF_SCREEN_THAT_TRIGGERS_CAMERA_MOVE;
            int moveTriggerScreenDistanceY = Screen.height / FRACTION_OF_SCREEN_THAT_TRIGGERS_CAMERA_MOVE;
            
            Vector3 moveCameraDelta = new Vector3();
            if (Input.mousePosition.x < moveTriggerScreenDistanceX)
                moveCameraDelta.x = Input.mousePosition.x / moveTriggerScreenDistanceX - 1; 
            if (Input.mousePosition.x > (FRACTION_OF_SCREEN_THAT_TRIGGERS_CAMERA_MOVE - 1) * moveTriggerScreenDistanceX)
                moveCameraDelta.x = Input.mousePosition.x / moveTriggerScreenDistanceX - (FRACTION_OF_SCREEN_THAT_TRIGGERS_CAMERA_MOVE - 1);
            if (Input.mousePosition.y < moveTriggerScreenDistanceY)
                moveCameraDelta.z = Input.mousePosition.y / moveTriggerScreenDistanceY - 1;
            if (Input.mousePosition.y > (FRACTION_OF_SCREEN_THAT_TRIGGERS_CAMERA_MOVE - 1) * moveTriggerScreenDistanceY)
                moveCameraDelta.z = Input.mousePosition.y / moveTriggerScreenDistanceY - (FRACTION_OF_SCREEN_THAT_TRIGGERS_CAMERA_MOVE - 1);
            
            if (moveCameraDelta == Vector3.zero || UIOnTheWay(Input.mousePosition))
                return;
            m_PlayerManager.CameraController.MoveCamera(moveCameraDelta);
        }

        void HandleCameraHeight()
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
                m_PlayerManager.CameraController.ChangeDistanceFromGround(-1f);
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f) 
                m_PlayerManager.CameraController.ChangeDistanceFromGround(1f);
        }

        void HandleCameraRotation()
        {
            if (Input.GetButton("Fire2") && !UIOnTheWay(Input.mousePosition))
            {
                if (!m_Mouse2Pressed)
                {
                    m_MousePositionCache = Input.mousePosition;
                    m_Mouse2Pressed = true;
                }
                else
                {
                    float deltaX = Input.mousePosition.y - m_MousePositionCache.y;
                    float deltaY = Input.mousePosition.x - m_MousePositionCache.x;
                    m_PlayerManager.CameraController.RotateCamera(deltaX, deltaY);
                    m_MousePositionCache = Input.mousePosition;
                }
            }
            else
                m_Mouse2Pressed = false;
        }

        bool IsMouseOnScreen()
        {
            return Input.mousePosition.y < Screen.height && Input.mousePosition.y > 0 &&
                   Input.mousePosition.x < Screen.width && Input.mousePosition.x > 0;
        }
    }
}
